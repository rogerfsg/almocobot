
var AppUtils = {

    criarBarril: function(idBarril, percentualCheio, larguraEmPx){

        larguraEmPx = larguraEmPx || 308;

        var alturaEmPx = larguraEmPx * 1.2;
        var alturaEfeitoOnda = alturaEmPx/10;
        var barrilSelector = "#" + idBarril;
        var nomeAnimacao = 'animacao-' + idBarril;

        $(barrilSelector + ' .container-barril').width(larguraEmPx);
        $(barrilSelector + ' .container-barril').height(alturaEmPx);

        $(barrilSelector + ' img.frame-barril').width(larguraEmPx);
        $(barrilSelector + ' img.frame-barril').height(alturaEmPx);

        $(barrilSelector + ' .efeito-topo-liquido').width(larguraEmPx);
        $(barrilSelector + ' .efeito-topo-liquido').height(alturaEfeitoOnda);
        $(barrilSelector + ' .efeito-topo-liquido').css({ 'background-size': (larguraEmPx*3) + 'px' + ' ' +  alturaEfeitoOnda + 'px'});

        $(barrilSelector + ' .liquido').width(larguraEmPx);
        $(barrilSelector + ' .liquido .bubble').css({'animation-name': nomeAnimacao, 'animation-iteration-count': 'infinite', 'animation-timing-function': 'linear'});

        var diametroBolha = larguraEmPx/10;
        var alturaTotal = $(barrilSelector + ' .container-barril').height();
        var alturaImagemEfeitoOnda = $(barrilSelector + ' .efeito-topo-liquido').height();

        var alturaLiquido = alturaTotal * (percentualCheio/100);
        var bottomImagemEfeitoOnda = percentualCheio == 100 ? alturaLiquido : alturaLiquido - alturaImagemEfeitoOnda;

        $(barrilSelector + ' .liquido .bubble').css({width: diametroBolha + 'px', height: diametroBolha + 'px', 'border-radius': (diametroBolha/2) + 'px'});
        AppUtils.insertStyleSheetRule("@keyframes " + nomeAnimacao + " { 0% { bottom: 0; } 50% { background-color: rgba(255, 255, 255, 0.2);  bottom: " + (bottomImagemEfeitoOnda/2) + "px; } 100% { background-color: rgba(255, 255, 255, 0); bottom: " + bottomImagemEfeitoOnda + "px; } }");

        var numeroBolhas = 5;
        var paddingBordaBarril = 10;
        var espacamentoEntreBolhas = ((larguraEmPx - (paddingBordaBarril*2)) - (diametroBolha*numeroBolhas)) / numeroBolhas;
        var leftAtual = paddingBordaBarril;

        for(var i=1; i <= numeroBolhas; i++)
        {
            $(barrilSelector + ' .bubble' + i).css({left: leftAtual + 'px' });
            leftAtual += diametroBolha + espacamentoEntreBolhas;
        }

        $(barrilSelector + ' .liquido')
            .delay(2000)
            .animate({
                height: alturaLiquido + 'px'
            }, 2500);

        $(barrilSelector + ' .efeito-topo-liquido')
            .delay(2000)
            .animate({
                bottom: bottomImagemEfeitoOnda + 'px',
                'background-position-x': '40px'
            }, 2500);

    },

    insertStyleSheetRule: function(ruleText)
    {
        var sheets = document.styleSheets;

        if(sheets.length == 0)
        {
            var style = document.createElement('style');
            style.appendChild(document.createTextNode(""));
            document.head.appendChild(style);
        }

        var sheet = sheets[sheets.length - 1];
        sheet.insertRule(ruleText, sheet.rules ? sheet.rules.length : sheet.cssRules.length);
    }

};