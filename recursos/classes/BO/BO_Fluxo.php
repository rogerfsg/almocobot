<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 17/04/2018
 * Time: 19:55
 */
class BO_Fluxo
{

    public static function factory(){
        return new BO_Fluxo();
    }

    public function executaAcao(){
        $classe = Registry::get('classe');
        $acao = Registry::get('acao');
        $objClasse = call_user_func_array(array($classe, "factory"), array());
        if (!is_object($objClasse))
        {
            throw new NotImplementedException ("Factory n�o foi definida para a classe {$classe}");
        }
        $retorno = call_user_func(array($objClasse, $acao));
        HelperLog::logInfo(print_r($retorno, true));
        return $retorno;
    }

    public function processo(){
        $input = Registry::get('inputFacebook');
        $message = $input['entry'][0]['messaging'][0];


        if(isset($message['delivery'])){

            return null;
        }else if(isset($message['postback'])){
            Registry::add(new Database(), 'Database');
            $postback = $message['postback'];

            BO_Interacao::registraInteracao($postback ['payload']);

            return $this->executaAcao();
        } else if(isset( $message['message']['quick_reply']['payload'])){
            Registry::add(new Database(), 'Database');


            if(!BO_Interacao::registraInteracao($message['message']['quick_reply']['payload'])){
                //quando for a segunda mensagem de um fluxo, vai sempre cair aqui
                $obj = BO_Interacao::carregaUltimaInteracao();
                HelperLog::logWebhook("Ultima iteracao: " .print_r($obj,true));
                if($obj != null){
                    return $this->executaAcao();

                } else {
                    return BO_Facebook::getMensagemInstrucaoAcessarMenuPersistente();
                }
            }else
                return $this->executaAcao();
        }
        else {
            Registry::add(new Database(), 'Database');
            //quando for a segunda mensagem de um fluxo, vai sempre cair aqui
            $obj = BO_Interacao::carregaUltimaInteracao();
            HelperLog::logWebhook("Ultima iteracao: " .print_r($obj,true));
            if($obj != null){
                return $this->executaAcao();

            } else {
                return BO_Facebook::getMensagemInstrucaoAcessarMenuPersistente();
            }
            //acesso direto no menu persistente
            //$retorno = call_user_func(array($this, $v[1]));
        }
    }

}