<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 17/04/2018
 * Time: 05:38
 */
class BO_Cliente
{
    public static function factory(){
        return new BO_Cliente();
    }
    public static function getIdCliente()
    {
        $db=Registry::get('Database');

        $senderId = Registry::get('senderId');
        $db->query("SELECT id FROM cliente WHERE sender_id = $senderId");
        $idRestaurante = $db->getPrimeiraTuplaDoResultSet(0);
        return $idRestaurante;
    }

    public static function editaCliente(){
        $db=Registry::get('Database');
        $senderId = Registry::get('senderId');
        $db->query("SELECT id FROM cliente WHERE sender_id = $senderId");
        $idRestaurante = $db->getPrimeiraTuplaDoResultSet(0);
        return $idRestaurante;
    }

    public static function inserirRestaurante($senderId)
    {
        $db=Registry::get('Database');
        $db->query("INSERT INTO restaurante (
sender_id
) 
VALUES (
$senderId
)");
        $idRestaurante = $db->getLastInsertId();
        return $idRestaurante;
    }


//    public function getEnderecoEntrega($texto = "Qual o endere�o de entrega?"){
//        BO_Interacao::registraInteracao("Cliente@BO_Cliente::setEnderecoDeEntrega");
//
//        $json = '{
//  "recipient":{
//    "id":"'.Registry::get('senderId'). '"
//  },
//  "message":{
//    "text": "'.$texto.'",
//    "quick_replies":[
//      {
//          "content_type":"location",
//        "title":"Minha posi��o",
//        "payload":"Cliente@BO_Pedido::getUltimosEnderecosDeEntrega"
//      }
//    ]
//  }
//}';
//        return utf8_encode( $json);
//    }

    public function getObjJson($idCliente)
    {
        $db=Registry::get('Database');
        $db->query("SELECT json FROM cliente WHERE id= $idCliente");
        $json= $db->getPrimeiraTuplaDoResultSet(0);
        $objJson=json_decode($json );
        $objJson=$objJson==null?new stdClass():$objJson;
        return $objJson;
    }

    public function updateJsonCliente($objJson){
        $db=Registry::get('Database');
        $jsonFrequencia=Helper::jsonEncode($objJson);
        $db->query("UPDATE cliente "
            ." SET json=".$db->formatarDados($jsonFrequencia)
            ." WHERE id=".Registry::get('idEntidade'));
    }

    public function setEnderecoDeEntrega(){

        $input = Registry::get('inputFacebook');
        $message = $input['entry'][0]['messaging'][0];
        HelperLog::logInfo("entrou");
        HelperLog::logInfo("Message: ".print_r($input['entry'][0],true));

        if(isset($message['message'] ['attachments'][0]['payload']['coordinates'])){
            BO_Interacao::registraInteracao("Restaurante@BO_Cliente::setComplementoDoEnderecoDeEntrega");
            //HelperLog::logWebhook("Message webhook: " .print_r($message,true));

            $coordinates=$message['message'] ['attachments'][0]['payload']['coordinates'];

            $db=Registry::get('Database');
            $jsonFrequencia=Helper::jsonEncode($message['message'] ['attachments'][0]);
            $db->query("UPDATE cliente "
                ." SET json_endereco_entrega=".$db->formatarDados($jsonFrequencia)
                .", lat_FLOAT=".Helper::formatarFloatParaComandoSQL( $coordinates['lat'])
                .", lng_FLOAT=".Helper::formatarFloatParaComandoSQL( $coordinates['lng'])
                ." WHERE id=".Registry::get('idEntidade'));

            return BO_Facebook::getJsonFacebook( "Qual o complemento do endere�o?");
        } else {

            //return $this->getEnderecoEntrega("Desculpa, precisamos do local de entrega para localizar os restaurantes nas proximidades.");
            $boPedido = new BO_Pedido();

            return $boPedido->getUltimosEnderecosDeEntrega("Desculpa, precisamos do local de entrega para localizar os restaurantes nas proximidades.");
        }

    }
    public function setComplementoDoEnderecoDeEntrega(){

        BO_Interacao::registraInteracao("Restaurante@BO_Cliente::escolherRestauranteProximoAMim");

        $objJson=$this->getObjJson(Registry::get('idEntidade'));
        $objJson->enderecoEntregaComplemento=Registry::get('messageFacebook');
        $this->updateJsonCliente($objJson);


        return $this->escolherRestauranteProximoAMim();

        //return BO_Facebook::getJsonFacebook(utf8_encode( "Tudo certo!"));
    }

    public function escolherRestauranteProximoAMim(){

        $res= $this->getRestaurantesProximosAMim();

        $limiteInicial = 0;
        if(Registry::contains('idAcao'))
            $limiteInicial = Registry::get('idAcao');

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');

        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();

        if(count($res) > 0){

            if(count($res) > 1){
                $ret->message->attachment->payload->template_type='list';
                $ret->message->attachment->payload->top_element_style='compact';
                $ps=array();

                for($i = 0 ; $i < count($res) && $i < BO_Facebook::LIMITE_LISTA; $i++, $limiteInicial++){
                    $restaurante = $res[$i];

                    $p = new stdClass();

                    $p->title = !strlen($restaurante->nome) ? "Restaurante $i" : $restaurante->nome;
                    $aceitaDelivery = false;
                    $msgFuncionamento =$this->getMensagemHorarioFuncionamento($aceitaDelivery, $restaurante);

                    $p->subtitle = $msgFuncionamento;

                    //Setando horario funcionamento
                    //Adicionar os horarios de funcionamento
                    if($aceitaDelivery){
                        $bs = array();
                        $b = new stdClass();
                        $b->title = "Editar";

                        $b->type="postback";
                        $b->payload="Cliente@BO_Pedido::verProdutosDoRestaurante?id_restaurante=".$restaurante->id . "&id_tipo_produto=".BO_Tipo_produto::PRATO;

                        $bs[count($bs)] = $b;

                        $p->buttons=$bs;
                    }

                    $ps[count($ps)] =$p;
                }
//        }
                $ret->message->attachment->payload->elements = $ps;

                if($limiteInicial  < count($res)){
                    $menu = array();
                    $b7=new stdClass();
                    $b7->title="Ver restante";
                    $b7->type="postback";
                    $b7->payload="Restaurante@BO_Pedido::menuHorarioFuncionamento?$limiteInicial";
                    $menu[count($menu)] = $b7;
                    $ret->message->attachment->payload->buttons=$menu;
                }

            } else{
                $restaurante = $res[0];
                $ret->message->attachment->payload->template_type='button';
                $ret->message->attachment->payload->text = !strlen($restaurante['nome']) ? "Restaurante 0" : $restaurante['nome'];

                $dados= BO_Restaurante::getDadosHorarioFuncionamentoHoje($restaurante['id'] );

                $aceitaDelivery = false;
                $msgFuncionamento =$this->getMensagemHorarioFuncionamento($aceitaDelivery, $dados);

                if($aceitaDelivery){
                    $ret->message->attachment->payload->text .= $msgFuncionamento;
                    $ret->message->attachment->payload->buttons=array();
                    $b = new stdClass();
                    $b->type='postback';
                    $b->payload="Cliente@BO_Pedido::verProdutosDoRestaurante?id_restaurante=".$restaurante['id']."&id_tipo_produto=".BO_Tipo_produto::PRATO;
                    $b->title="Ver card�pio";
                    $ret->message->attachment->payload->buttons[0]=$b;
                } else {
                    return BO_Facebook::getJsonFacebook($msgFuncionamento);
                }
            }

            return Helper::jsonEncode($ret);
        } else{
            return BO_Facebook::getJsonFacebook("Ainda n�o temos restaurantes conveniados na sua regi�o.");
        }
    }

    public function getMensagemHorarioFuncionamento(&$aceitaDelivery, $dados){
        $aceitaDelivery = false;
        $horaAtual = Helper::getHoraAtual();
        $msg="";
        if(strlen($dados['horarioInicio'])
            && ($horaAtual >= $dados['horarioInicio']
                && $horaAtual <= $dados['horarioFim'])){
            $aceitaDelivery = true;
            return "Aceita pedido de delivery das "
                .$dados['horarioInicio']." �s ".$dados['horarioFim']." horas.";
        } else {
            if(!strlen($dados['horarioInicio'])){
                return "Hoje n�o aceita delivery por aqui!";
            }
            else if($horaAtual < $dados['horarioInicio'])
                return "Daqui a pouco voc� j� pode pedir "
                    .$dados['horarioInicio']." �s ".$dados['horarioFim']." horas.";
            else
                return "Hoje n�o aceitam mais pedidos, o hor�rio de pedido do delivery � "
                    .$dados['horarioInicio']." �s ".$dados['horarioFim']." horas.";
        }
    }

    public function getMinhaLocalizacao(){
        $db = Registry::get('Database');
        $db->query("SELECT lat_FLOAT, lng_FLOAT FROM cliente WHERE id = ".Registry::get('idEntidade'));
        $m=Helper::getResultSetToMatriz($db->result);
        return $m[0];
    }

    public function getRestaurantesProximosAMim(){

        $db = Registry::get('Database');
        $db->query("SELECT id, nome, lat_FLOAT lat, lng_FLOAT lng, json FROM restaurante");
        $matriz = Helper::getResultSetToMatriz($db->result);

        $minhaLocalizacao= $this->getMinhaLocalizacao();
        $res = array();
        for($i=0;$i<count($matriz );$i++){
            $obj = $matriz[$i];
            if($obj['lat'] != null && $minhaLocalizacao['lng'] != null){
                $distance = Helper::haversineGreatCircleDistance(
                    $obj['lat'], $obj['lng'], $minhaLocalizacao['lat'], $minhaLocalizacao['lng']);
            }

            HelperLog::logInfo('A distancia � de: '.$distance);

            $objJson= json_decode($obj['json']);
            if($objJson != null){
                if($distance <= $objJson->raioCoberturaMetros)
                    $res[count($res)] = $obj;
            }
        }
        return $res;
    }


    //Prin



    public static function inserir($senderId)
    {
        $db=Registry::get('Database');
        $db->query("INSERT INTO cliente (
sender_id
) 
VALUES (
$senderId
)");
        $idCliente= $db->getLastInsertId();
        return $idCliente;
    }

    public function verPratos(){
        $idPedido=null;
        if(Registry::contains('parametros')){
            $p=Registry::get('parametros');
            if(isset($p['id_pedido'])){
                $idPedido=$p['id_pedido'];
            }
        }

        if(isset($idPedido)){
            $boPedido = new BO_Pedido();
            return $boPedido->verProdutosDoRestaurante($idPedido);
        } else {
            return $this->novoPedido($idPedido);
        }
    }

    public function novoPedido($idPedido=null){
        $boPedido  = new BO_Pedido();
        $ultimoPedido= $boPedido->verUltimoPedidoEmAbertoSeExistente();
        if($ultimoPedido!=null){
            return $ultimoPedido;
        }
        if($idPedido==null){
            return $boPedido->getUltimosEnderecosDeEntrega();
        }

        return BO_Cliente::escolherRestauranteProximoAMim();
    }

    public function verBebidas(){
        $idPedido=null;
        if(Registry::contains('parametros')){
            $p=Registry::get('parametros');
            if(isset($p['id_pedido'])){
                $idPedido=$p['id_pedido'];
            }
        }

        $ultimoPedido= BO_Pedido::verUltimoPedidoEmAbertoSeExistente();
        if($ultimoPedido!=null){
            return $ultimoPedido;
        }
        if($idPedido==null){
            $boPedido = new BO_Pedido();
            return $boPedido->getUltimosEnderecosDeEntrega();
        }

        return BO_Cliente::getRestaurantesProximosAMim();
    }

    public function verPedidos(){
        $parametros =Registry::get('parametros');
        if(isset($parametros['id_inferior']))
            $idInferior = $parametros['id_inferior'];
        else $idInferior =0;

        $q= "SELECT p.id, p.estado_pedido_id_INT idEstadoPedido, p.json_produtos, r.nome restaurante
        FROM pedido p JOIN restaurante r ON p.restaurante_id_INT = r.id
        WHERE cliente_id_INT = ".Registry::get('idEntidade')."
        ORDER BY id DESC 
        LIMIT $idInferior , ".BO_Facebook::LIMITE_LISTA;

        $db = Registry::get('Database');
        $db->query($q);
        $pedidos= Helper::getResultSetToMatriz($db->result);


        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');

        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();

        if(count($pedidos)){
            if(count($pedidos)==1){
                $pedido = $pedidos[0];
                $ret->message->attachment->payload->template_type='generic';
                $e=new stdClass();
                $e->title = "Pedido ".$pedido['id'];
                $e->subtitle= $pedido['restaurante']." ".BO_Estado_pedido::getDescricao($pedido['idEstadoPedido']);
                $b = new stdClass();
                $b->type="postback";
                $b->payload="Cliente@BO_Pedido::verDetalhesPedido";
                $b->title = "+info";
                $e->buttons = array($b);
                $ret->message->attachment->payload->elements=array($e);
                return Helper::jsonEncode($ret);
            } else{

                $ret->message->attachment->payload->template_type='list';
                $ret->message->attachment->payload->top_element_style='compact';
                $es=array();
                for($i = 0 ; $i < count($pedidos); $i++){
                    $pedido = $pedidos[$i];
                    $e=new stdClass();
                    $e->title = "Pedido ".$pedido['id'];
                    $e->subtitle= $pedido['restaurante'].". Status: ".BO_Estado_pedido::getDescricao($pedido['idEstadoPedido']);
                    $b = new stdClass();
                    $b->type="postback";
                    $b->payload="Cliente@BO_Pedido::verDetalhesPedido?id_pedido={$pedido['id']}";
                    $b->title = "+info";
                    $e->buttons = array($b);
                    $es[count($es)]=$e;
                }

                $ret->message->attachment->payload->elements=$es;
                return Helper::jsonEncode( $ret);
            }
        } else {
            return BO_Facebook::getJsonFacebookDaMensagem("Voc� ainda n�o realizou nenhum pedido por aqui...");
        }
    }


}