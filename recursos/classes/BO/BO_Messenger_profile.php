<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 17/04/2018
 * Time: 20:02
 */
class BO_Messenger_profile
{
    public static function factory(){
        return new BO_Messenger_profile();
    }

    public function getUserProfile()
    {
        $senderId = Registry::get('senderId');

        //curl -X GET "https://graph.facebook.com/v2.6/<PSID>?fields=first_name,last_name,profile_pic&access_token=<PAGE_ACCESS_TOKEN>
        $data = Helper::curlFacebook(
            "https://graph.facebook.com/v2.6/$senderId?fields=first_name,last_name,profile_pic&access_token=".FACEBOOK_API_KEY
            , null);

    }

    public function menuPersistente(){
        $json='{
             "get_started":{
                "payload":"START"
              },
              "persistent_menu":[
                {
                  "locale":"default",
                  "composer_input_disabled": false,
                  "call_to_actions":[
                  
                    {
                      "title":"Pratos a venda",
                      "type":"nested",
                      "call_to_actions":[
                        {
                          "title":"Ver todos",
                          "type":"postback",
                          "payload":"Restaurante@BO_Produto::verPratos"
                        },
                        {
                          "title":"Cadastrar",
                          "type":"postback",
                          "payload":"Restaurante@BO_Produto::cadastrarPrato"
                        },
                        {
                          "title":"Pratos de hoje",
                          "type":"postback",
                          "payload":"Restaurante@BO_Produto::verPratosDeHoje"
                        }
                      ]
                    },
                    {
                      "title":"Bebidas a venda",
                      "type":"nested",
                      "call_to_actions":[
                        {
                          "title":"Ver todas",
                          "type":"postback",
                          "payload":"Restaurante@BO_Produto::verBebidas"
                        },
                        {
                          "title":"Cadastrar",
                          "type":"postback",
                          "payload":"Restaurante@BO_Produto::cadastrarBebida"
                        }
                      ]
                    },
                    {
                      "title":"Meu local",
                      "type":"nested",
                      "call_to_actions":[
                        {
                          "title":"Meus dados",
                          "type":"postback",
                          "payload":"Restaurante@BO_Restaurante::editarMeusDados"
                        },
                        {
                          "title":"Hor�rio funcionamento",
                          "type":"postback",
                          "payload":"Restaurante@BO_Restaurante::menuHorarioFuncionamento"
                        },
                        {
                          "title":"Pedidos de delivery",
                          "type":"postback",
                          "payload":"Restaurante@BO_Pedidos::verPedidos"
                        },
                      ]
                    }
                  ]
                }
              ]
            }';
        Helper::curlFacebook(
            "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=" . FACEBOOK_API_KEY,
            utf8_encode($json)
        );


    }




    public function menuPersistenteCliente(){
        $json='{
             "get_started":{
                "payload":"START"
              },
              "persistent_menu":[
                {
                  "locale":"default",
                  "composer_input_disabled": false,
                   "call_to_actions":[
                    
                    {
                      "title":"Pratos a venda",
                      "type":"postback",
                      "payload":"Cliente@BO_Cliente::verPratos"
                    },
                    {
                      "title":"Bebidas a venda",
                      "type":"postback",
                      "payload":"Cliente@BO_Cliente::verBebidas"
                    },
                    {
                      "title":"Meus pedidos",
                      "type":"postback",
                      "payload":"Cliente@BO_Cliente::verPedidos"
                    }
                 
                  ]
                }
              ]
            }';
        Helper::curlFacebook(
            "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=" . FACEBOOK_API_KEY,
            utf8_encode($json)
        );


    }
}