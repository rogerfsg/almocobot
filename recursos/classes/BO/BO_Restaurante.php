<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 17/04/2018
 * Time: 05:38
 */
class BO_Restaurante
{
    public static function factory(){
        return new BO_Restaurante();
    }

    public static function getNomeRestaurante($idRestaurante)
    {
        $db=Registry::get('Database');
        $db->query("SELECT nome FROM restaurante WHERE id = $idRestaurante");
        $idRestaurante = $db->getPrimeiraTuplaDoResultSet(0);
        return $idRestaurante;
    }
    public static function getIdRestaurante($senderId)
    {
        $db=Registry::get('Database');
        $db->query("SELECT id FROM restaurante WHERE sender_id = $senderId");
        $idRestaurante = $db->getPrimeiraTuplaDoResultSet(0);
        return $idRestaurante;
    }

    public function getObjJson($idRestaurante)
    {
        $db=Registry::get('Database');
        $db->query("SELECT json FROM restaurante WHERE id= $idRestaurante");
        $jsonRestaurante = $db->getPrimeiraTuplaDoResultSet(0);
        $objJson= json_decode($jsonRestaurante );
        $objJson=$objJson==null?new stdClass():$objJson;
        return $objJson;
    }

    public static function inserirRestaurante($senderId)
    {
        $db=Registry::get('Database');
        $db->query("INSERT INTO restaurante (
sender_id
) 
VALUES (
$senderId
)");
        $idRestaurante = $db->getLastInsertId();
        return $idRestaurante;
    }

    public function getSubtitle(  $idDiaSemana){
        //Setando horario funcionamento
        $subtitle ="";
        if(BO_Frequencia::isDiaDaSemana($idDiaSemana)){
            $sigla = BO_Frequencia::getSigla($idDiaSemana);
            HelperLog::logInfo("Dia semana: $idDiaSemana. ");
            $q = "SELECT inicio_{$sigla}_TIME horarioInicio, fim_{$sigla}_TIME horarioFim FROM restaurante WHERE id = ".Registry::get('idEntidade');
            $db = Registry::get('Database');
            $db->query($q);
            $m= Helper::getResultSetToMatriz($db->result);
            $validade = false;
            if(count($m) > 0){
                $objHorario= $m[0];
                if(strlen($objHorario['horarioInicio'])){
                    $subtitle .= $objHorario['horarioInicio'];
                    $validade =true;
                }

                if(strlen($objHorario['horarioFim'])){
                    if(strlen($subtitle))$subtitle.=" �s ";
                    $subtitle .= $objHorario['horarioFim'];
                    $validade =true;
                }
            }

            if(!$validade)
                $subtitle.="N�o trabalho nesse dia";
            else $subtitle .= " horas aceita delivery";
        }else {
            $subtitle .= "Editar todos de uma s� vez";
        }
        return $subtitle;
    }

    public function menuHorarioFuncionamento(){
//        $db=Registry::get('Database');
        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::menuHorarioFuncionamento");

        $limiteInicial = 0;
        if(Registry::contains('idAcao'))
            $limiteInicial = Registry::get('idAcao');

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='list';
        $ret->message->attachment->payload->top_element_style='compact';

        $ps=array();
        $labels= BO_Frequencia::getLabels();

        $objJson=BO_Restaurante::getObjJson(Registry::get('idEntidade'));

//        for($x=0;$x<count($labels);){
        $limite=$limiteInicial + BO_Facebook::LIMITE_LISTA;
        $idsFrequencia = BO_Frequencia::getIdsFrequencia();
        for($i = $limiteInicial ;
            $i < count($idsFrequencia)
            && $i< $limite;
            $i++,$limiteInicial++) {

            $idDiaSemana = $idsFrequencia[$i];

            $p = new stdClass();

            $p->title = $labels[$i];

            //Setando horario funcionamento
            $p->subtitle = $this->getSubtitle( $idDiaSemana);

            //Adicionar os horarios de funcionamento
            $bs = array();
            $b = new stdClass();
            $b->title = "Editar";

            $b->type="postback";
            $b->payload="Restaurante@BO_Restaurante::editarHorarioFuncionamento?".$idDiaSemana;

            $bs[count($bs)] = $b;

            $p->buttons=$bs;
            $ps[count($ps)] =$p;
        }
//        }
        $ret->message->attachment->payload->elements = $ps;

        if($limite < count($labels)){
            $menu = array();
            $b7=new stdClass();
            $b7->title="Ver restante";
            $b7->type="postback";
            $b7->payload="Restaurante@BO_Restaurante::menuHorarioFuncionamento?$limiteInicial";
            $menu[count($menu)] = $b7;
            $ret->message->attachment->payload->buttons=$menu;
        }

        return Helper::jsonEncode($ret);
    }


    public function editarHorarioFuncionamento(){

        $idFrequencia = Registry::get('idAcao');
//        Helper::logInfo("PASSOU");
        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::setarHorarioInicio?$idFrequencia");

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='button';

        if($idFrequencia==BO_Frequencia::SEGUNDA_A_SEXTA){
            $ret->message->attachment->payload->text=
                "De ".BO_Frequencia::getLabelCompleta($idFrequencia)
                . ", voc� come�a que horas?";
        }else{
            $ret->message->attachment->payload->text=
                "Na ".BO_Frequencia::getLabelCompleta($idFrequencia)
                . ", voc� come�a que horas?";
        }

        $b1=new stdClass();

        $b1->title= "N�o trabalho";
        $b1->type="postback";
        $b1->payload="Restaurante@BO_Restaurante::naoTrabalhoNesseDia?".$idFrequencia;
        $bs=array();
        $bs[count($bs)] = $b1;

        $ret->message->attachment->payload->buttons=$bs;

        return Helper::jsonEncode($ret);
    }

    private function initIdDiaSemana($idDiaSemana, $isHorarioInicio=true){

        $aux=null;
        $horario= BO_Facebook::getTimeDaMensagem();

        $strUpdate = "";

        switch ($idDiaSemana){
            case BO_Frequencia::SEGUNDA_A_SEXTA:
                $idsFrequencia=BO_Frequencia::getIdsSegundaASexta();

                foreach ($idsFrequencia as $idDiaSemana){
                    $sigla = BO_Frequencia::getSigla($idDiaSemana);
                    if(strlen($strUpdate)) $strUpdate.=", ";
                    if($isHorarioInicio)
                        $strUpdate .= " inicio_{$sigla}_TIME = '{$horario}' ";
                    else
                        $strUpdate .= " fim_{$sigla}_TIME = '{$horario}' ";
                }
                break;
            default:
                $sigla = BO_Frequencia::getSigla($idDiaSemana);
                if($isHorarioInicio)
                    $strUpdate .= " inicio_{$sigla}_TIME = '{$horario}' ";
                else
                    $strUpdate .= " fim_{$sigla}_TIME = '{$horario}' ";
                break;
        }

        $db=Registry::get('Database');
        $db->query("UPDATE restaurante "
            ." SET $strUpdate "
            ." WHERE id=".Registry::get('idEntidade'));
    }

    public function getIndexDiaTrabalho($v, $idDiaSemana){
        for($i=0;$i<count($v); $i++){
            if($v[$i]==$idDiaSemana)
                return $i;
        }
        return null;
    }

    public function setarHorarioInicio(){
        $idFrequencia = Registry::get('idAcao');
        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::setarHorarioFim?$idFrequencia");

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='button';
        $ret->message->attachment->payload->text= "Para de aceitar pedidos de delivery que horas?";

        $this->initIdDiaSemana($idFrequencia, true);

        $bs=array();

        $b1=new stdClass();
        $b1->title= "N�o trabalho";
        $b1->type="postback";
        $b1->payload="Restaurante@BO_Restaurante::naoTrabalhoNesseDia?".$idFrequencia;
        $bs[count($bs)] = $b1;

        $ret->message->attachment->payload->buttons=$bs;

        return Helper::jsonEncode($ret);

    }

    public function setarHorarioFim(){
        $idFrequencia = Registry::get('idAcao');
        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::setarHorarioFim?$idFrequencia");

        $this->initIdDiaSemana($idFrequencia, false);

        $subtitle=null;

        $subtitle=BO_Frequencia::isDiaDaSemana($idFrequencia)
            ? $this->getSubtitle( $idFrequencia )
            :  'Hor�rio de funcionamento atualizado!';
        return BO_Facebook::getJsonFacebook($subtitle);
    }


    public function getEnderecoEntrega($texto = "Qual o endere�o de entrega? Clique no bot�o abaixo..."){
        BO_Interacao::registraInteracao("Restaurante@BO_Cliente::setEnderecoDeEntrega");

        $json = '{
  "recipient":{
    "id":"'.Registry::get('senderId'). '"
  },
  "message":{
    "text": "'.$texto.'",
    "quick_replies":[
      {
          "content_type":"location",
        "title":"Minha posi��o",
        "payload":"Restaurante@qualRaioDeCobertura"
      }
    ]
  }
}';
        return utf8_encode( $json);
    }
    public function editarMeusDados(){

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->text= "Qual o endere�o do seu restaurante?";
        $objJson=$this->getObjJson(Registry::get('idEntidade'));
        if($objJson!= null && isset($objJson->endereco)){
            $ret->message->text .= " Endere�o atual: " . $objJson->endereco;
        }
        //A acao location n�o retorna payload, logo temos que registrar a proxima acao
        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::setEndereco");
        $ret->message->quick_replies=array();
        $ret->message->quick_replies[0]=new stdClass();
        $ret->message->quick_replies[0]->content_type="text";
        $ret->message->quick_replies[0]->title="Minha posi��o";
        $ret->message->quick_replies[0]->payload="Restaurante@BO_Restaurante::setEndereco";

//        $bs=array();
        if($objJson->endereco!= null){
            $ret->message->quick_replies[1]=new stdClass();
            $ret->message->quick_replies[1]->content_type="text";
            $ret->message->quick_replies[1]->title="Manter";
            $ret->message->quick_replies[1]->payload="Restaurante@BO_Restaurante::qualRaioDeCobertura";
        }
        $idNext = count($ret->message->quick_replies) + 1;
        $ret->message->quick_replies[$idNext]=new stdClass();
        $ret->message->quick_replies[$idNext]->content_type="location";
//        $b2=new stdClass();
//        if(isset($objJson->ativado) && $objJson->ativado == 1)
//            $b2->title= "Desativar delivery bot";
//        else $b2->title= "Ativar delivery bot";
//        $b2->type="postback";
//        $acaoAtivar= $objJson->ativado ? 0 : 1;
//        $b2->payload="Restaurante@BO_Restaurante::setAtivarOuDesativar?".$acaoAtivar;
//        $bs[count($bs)] = $b2;
//
//        $ret->message->attachment->payload->buttons=$bs;

        return Helper::jsonEncode($ret);

    }


    public function setEndereco(){

        $input = Registry::get('inputFacebook');
        $message = $input['entry'][0]['messaging'][0];
        if(isset($message['message'] ['attachments'][0]['payload']['coordinates'])){

            BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::setarComplemento");

            $objJson=$this->getObjJson(Registry::get('idEntidade'));

            $objJson->objEndereco = $message['message'] ['attachments'][0];
            $db=Registry::get('Database');
            $jsonFrequencia=Helper::jsonEncode($objJson);
            $lat=  $message['message'] ['attachments'][0]['payload']['coordinates']['lat'];
            $long= $message['message'] ['attachments'][0]['payload']['coordinates']['long'];
            $db->query("UPDATE restaurante 
                     SET json=".$db->formatarDados($jsonFrequencia).",
                        lat = ".Helper::formatarFloatParaComandoSQL($lat).", 
                        lng = ".Helper::formatarFloatParaComandoSQL($long)."
                     WHERE id=".Registry::get('idEntidade'));

            return BO_Facebook::getJsonFacebook("Qual o complemento do endere�o?");
        } else {

            return $this->getEnderecoEntrega("Desculpa, precisamos do local de entrega para localizar os restaurantes nas proximidades.");
        }
    }


    public function setarComplemento()
    {

        $objJson = $this->getObjJson(Registry::get('idEntidade'));

        if (!Registry::contains('idAcao') || Registry::get('idAcao') != 0) {

            $objJson->enderecoComplemento = Registry::get('messageFacebook');
            $this->updateJson($objJson);
        }
        return $this->qualRaioDeCobertura();
    }

    public function qualRaioDeCobertura(){


        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::setarRaioDeCobertura");

        $objJson= $this->getObjJson(Registry::get('idEntidade'));

        if(strlen($objJson->raioCoberturaMetros)){

            $ret = new stdClass();
            $ret->recipient=new stdClass();
            $ret->recipient->id=Registry::get('senderId');

            $ret->message=new stdClass();
            $ret->message->attachment=new stdClass();
            $ret->message->attachment->type='template';
            $ret->message->attachment->payload=new stdClass();
            $ret->message->attachment->payload->template_type='button';
            $ret->message->attachment->payload->text= "Qual o raio de cobertura em metros do delivery?";
            $ret->message->attachment->payload->text.= " O atual � de ".$objJson->raioCoberturaMetros." metros";

            $bs=array();
            $b1=new stdClass();

            $b1->title= "Manter";
            $b1->type="postback";
            $b1->payload="Restaurante@BO_Restaurante::setarRaioDeCobertura?0";
            $bs[count($bs)] = $b1;

            $ret->message->attachment->payload->buttons=$bs;
            return Helper::jsonEncode($ret);
        } else {
            return BO_Facebook::getJsonFacebook("Qual o raio de cobertura em metros do delivery?");
        }



    }

    public function setarRaioDeCobertura(){
        $objJson=$this->getObjJson(Registry::get('idEntidade'));

        if(!Registry::contains('idAcao') || Registry::get('idAcao') != 0){

            $objJson->raioCoberturaMetros=Registry::get('messageFacebook');
            $this->updateJson($objJson);
        }
        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::setarTelefone");

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');

        $ret->message=new stdClass();
        $ret->message->text= "Qual o telefone para contato dos clientes?";

        $ret->message->quick_replies=array();
        $ret->message->quick_replies[0]=new stdClass();
        $ret->message->quick_replies[0]->content_type="user_phone_number";
        $ret->message->quick_replies[0]->title="Enviar n�mero";
        $ret->message->quick_replies[0]->payload="Restaurante@BO_Restaurante::setAtivarOuDesativar";

//        $bs=array();
        if($objJson->telefone!= null){
            $ret->message->text .= " O atual �: ".$objJson->telefone;

            $ret->message->quick_replies[1]=new stdClass();
            $ret->message->quick_replies[1]->content_type="text";
            $ret->message->quick_replies[1]->title="Manter";
            $ret->message->quick_replies[1]->payload="Restaurante@BO_Restaurante::setarTelefone";
        }

        return Helper::jsonEncode($ret);
    }

    public function updateJson($objJson){
        $db=Registry::get('Database');
        $jsonFrequencia=Helper::jsonEncode($objJson);
        $db->query("UPDATE restaurante "
            ." SET json=".$db->formatarDados($jsonFrequencia)
            ." WHERE id=".Registry::get('idEntidade'));
    }

    public function setarTelefone(){
        $objJson=$this->getObjJson(Registry::get('idEntidade'));

        if(!Registry::contains('idAcao')
            || Registry::get('idAcao') != 0){

            $number= BO_Facebook::getPhoneNumberDaMensagem();

            $objJson->telefone=$number;
            $this->updateJson($objJson);
        }

        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::finalizarEdicaoCadastro");

        return BO_Facebook::getJsonFacebook("Dados cadastrados com sucesso!");

    }

    public function finalizarEdicaoCadastro(){
        return BO_Facebook::getJsonFacebook("Dados cadastrados com sucesso!");
    }
    public function ativarOuDesativar(){

        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::setAtivarOuDesativar");

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='button';

        $objJson=$this->getObjJson(Registry::get('idEntidade'));
        if($objJson!=null && $objJson->ativado){
            $ret->message->attachment->payload->text= "Deseja desativar seu restaurante do Bot?";
        } else {
            $ret->message->attachment->payload->text= "Deseja ativar seu restaurante do Bot?";
        }
//        HelperLog::logInfo('saiu editarHorarioFuncionamento: '.print_r($ret,true));
        $bs=array();

        $b1=new stdClass();
        $b1->type="postback";
        if($objJson!=null && $objJson->ativado == 1){
            $b1->title= "Desativar";
            $b1->payload="Restaurante@BO_Restaurante::setAtivarOuDesativar?0";
        } else {
            $b1->title= "Ativar";
            $b1->payload="Restaurante@BO_Restaurante::setAtivarOuDesativar?1";
        }

        $bs[count($bs)] = $b1;

        $b2 = new stdClass();
        $b2->type="postback";
        $b2->title= "Manter";
        $b2->payload="Restaurante@BO_Restaurante::finalizarEdicaoCadastro";

        $bs[count($bs)] = $b2;

        $ret->message->attachment->payload->buttons=$bs;

        return Helper::jsonEncode($ret);
    }

    public function setAtivarOuDesativar(){
        $desc="Dados salvos com sucesso!";
        if(Registry::contains('idAcao') ){
            $objJson=$this->getObjJson(Registry::get('idEntidade'));

            $objJson->ativado=Registry::get('idAcao');
            $desc=$objJson->ativado != null && $objJson->ativado ==1
                ? "Bot ativado com sucesso! O seu restaurante agora est� apto a receber pedidos de delivery nos dias de funcionamento!"
                : "Bot desativado com sucesso, voc� n�o receber� nenhum pedido de delivery por aqui enquanto n�o ativar novamente.";

            $this->updateJson($objJson);
        }


        BO_Interacao::registraInteracao("Restaurante@BO_Restaurante::finalizarEdicaoCadastro");

        return BO_Facebook::getJsonFacebook($desc);
    }

    public function getRestaurantesProximos(){
        $db = Registry::get('Database');
        $db->query("SELECT json FROM restaurante");
    }


    public static function getDadosHorarioFuncionamentoHoje(  $idRestaurante){

        $idDiaSemana  = Helper::getDiaDaSemana();
        //Setando horario funcionamento

//        if(BO_Frequencia::isDiaDaSemana($idDiaSemana)){
        $sigla = BO_Frequencia::getSigla($idDiaSemana);

        $q = "SELECT inicio_{$sigla}_TIME horarioInicio, fim_{$sigla}_TIME horarioFim
 FROM restaurante
 WHERE id = ".$idRestaurante;
        $db = Registry::get('Database');
        $db->query($q);
        $m= Helper::getResultSetToMatriz($db->result);

        if(count($m) > 0){
            return $m[0];
        }
        return null;
//        } else return null;
//        return $subtitle;
    }

}