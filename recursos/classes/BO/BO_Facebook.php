<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 19/04/2018
 * Time: 18:13
 */
class BO_Facebook
{
    const LIMITE_LISTA=4;


    public static function getTimeDaMensagem(){

        $input = Registry::get('inputFacebook');
        if(isset($input['entry'][0]['messaging'][0]['message']['nlp'])){
            $nlp =$input['entry'][0]['messaging'][0]['message']['nlp'];
            if(count($nlp['entities']['datetime'])>0){
                $index= -1;
                if(count($nlp['entities']['datetime'][0]['values']) > 1)
                    $index=1;
                else $index=0;
                if($index >=0){
                    $datetime= $nlp['entities']['datetime'][0]['value'][$index];
                    HelperLog::logInfo(print_r("Data: ",true));
                    HelperLog::logInfo(print_r($datetime,true));
                    $v=Helper::explode('[T]', $datetime);
                    HelperLog::logInfo("asdf");
                    HelperLog::logInfo(print_r($v,true));
                    return $v[1];
                }
            }
        }

        $hora = Registry::get('messageFacebook');

        $hora = Helper::somenteNumeros($hora );

        if(strlen($hora) == 2)
            return $hora.":00";
        else if($hora === 0 || ($hora > 0 && $hora < 10))
            return "0{$hora}:00";
        else if(strlen($hora) > 2)
            return $hora;
        else return null;
    }

    public static function getNumeroDaMensagem(){

        $num = Registry::get('messageFacebook');

        $num = Helper::somenteNumeros($num);

        if(strlen($num)) return $num;
        else return null;
    }

    public static function getPhoneNumberDaMensagem(){

        $input = Registry::get('inputFacebook');
        if(isset($input['entry'][0]['messaging'][0]['message']['nlp'])){
            $nlp =$input['entry'][0]['messaging'][0]['message']['nlp'];
            if(count($nlp['entities']['phone_number'])>0){
                return $nlp['entities']['phone_number'][0]['value'];
            }
        }

        $num = Registry::get('messageFacebook');

        $num = Helper::somenteNumeros($num);

        if(strlen($num)) return $num;
        else return null;
    }

    public static function getPrecoDaMensagem(){

        $input = Registry::get('inputFacebook');
        if(isset($input['entry'][0]['messaging'][0]['message']['nlp'])){
            $nlp =$input['entry'][0]['messaging'][0]['message']['nlp'];
            if(count($nlp['entities']['amount_of_money'])>0){
                return $nlp['entities']['amount_of_money'][0]['value'];
            }
        }

        $preco = Registry::get('messageFacebook');
//        HelperLog::logInfo("Preco 1: $preco");
        $preco = str_replace(',','.',$preco);
//        HelperLog::logInfo("Preco 2: $preco");
        $preco = Helper::somenteNumerosEPonto($preco);

        if(strlen($preco)) return $preco;
        else return null;
    }
    public static function getMensagemInstrucaoPreco(){
        $senderId = Registry::get('senderId');
        $jsonData = '{
            "messaging_type": "RESPONSE",
            "recipient":{
                "id": "' . $senderId . '"
            }, 
            "message":{
                "text": "Desculpa n�o entendi! Tente da seguinte maneira R$ 2,40"
            }
        }';
        return $jsonData;
    }

    public static function getMensagemInstrucaoAcessarMenuPersistente(){
        $senderId = Registry::get('senderId');
        $jsonData = '{
            "messaging_type": "RESPONSE",
            "recipient":{
                "id": "' . $senderId . '"
            }, 
            "message":{
                "text": "Desculpa n�o entendi! Acessa o menu aqui embaixo para gente come�ar denovo"
            }
        }';
        return $jsonData;
    }

    public static function getJsonFacebook($texto){
        $texto = utf8_encode($texto);
        $jsonData = '{
            "messaging_type": "RESPONSE",
            "recipient":{
                "id": "' . Registry::get('senderId') . '"
            }, 
            "message":{
                "text": "'.$texto.'"
            }
        }';
        return $jsonData;
    }



    public static function getJsonFacebookDaMensagem($msg){
        if(Interface_mensagem::checkErro($msg)){
            HelperLog::logErro(null,$msg);
            return BO_Facebook::getJsonFacebook(
                "Desculpe ocorreu um erro, j� avisamos nossa equipe. Tente novamente, caso persista j� estamos olhando para corrigir!");
        } else {
            return BO_Facebook::getJsonFacebook($msg->mMensagem);
        }
    }
}