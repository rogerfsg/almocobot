<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 23/04/2018
 * Time: 11:59
 */
class BO_Pedido
{

    public static function getIdRestaurante($idPedido)
    {
        $db=Registry::get('Database');
        $db->query("SELECT restaurante_id_INT FROM pedido WHERE id = $idPedido");
        $idRestaurante = $db->getPrimeiraTuplaDoResultSet(0);
        return $idRestaurante;
    }

    public static function getNomeRestaurante($idPedido)
    {
        $db=Registry::get('Database');
        $db->query("SELECT r.nome FROM pedido p join restaurante r ON p.restaurante_id_INT = r.id WHERE p.id = $idPedido");
        $idRestaurante = $db->getPrimeiraTuplaDoResultSet(0);
        return $idRestaurante;
    }
    public static function getIdEstadoPedido($idPedido)
    {
        $db=Registry::get('Database');
        $db->query("SELECT p.estado_pedido_id_INT FROM pedido p  WHERE p.id = $idPedido");
        $idRestaurante = $db->getPrimeiraTuplaDoResultSet(0);
        return $idRestaurante;
    }
    public static function factory(){
        return new BO_Pedido();
    }

    public function escolherQuantidadePrato(){

        $strParametros = Registry::get('strParametros');
        BO_Interacao::registraInteracao("Cliente@BO_Pedido::setEscolherQuantidadePrato?$strParametros");

        return BO_Facebook::getJsonFacebook("Qual a quantidade? 1, 2, 5?");
    }

    public function setEscolherQuantidadePrato(){
        $qtd= BO_Facebook::getNumeroDaMensagem();
        if($qtd == null){
            return BO_Facebook::getJsonFacebook("Desculpe, n�o entendi a quantidade. Tente usar apenas n�meros por favor. Ex: 1, 2, 8");
        }

        $parametros = Registry::get('parametros');
        $idPedido =$parametros['id_pedido'];
        $idProduto=$parametros['id_produto'];
//        $idPedido = Registry::get('idAcao');

        $db = Registry::get('Database');

        $q = "SELECT id, json_produtos, json_iteracoes, preco, estado_pedido_id_INT, cliente_id_INT, psid, restaurante_id_INT
            FROM pedido 
            WHERE id= $idPedido";

        $db->query($q);

        $pedidos= Helper::getResultSetToMatriz($db->result);
        $jsonProdutos= $pedidos[0]['json_produtos'];
        $objProdutos= json_decode($jsonProdutos);
        if($objProdutos == null) {
            $objProdutos = new stdClass();
        }
        if(!isset($objProdutos->$idProduto))
            $objProdutos->$idProduto = $qtd;
        else $objProdutos->$idProduto += $qtd;

        $jsonProdutos = Helper::jsonEncode($objProdutos);
        $q = "UPDATE pedido SET json_produtos = ".$db->formatarDados($jsonProdutos)." WHERE id= $idPedido";
        $db->query($q);
        return $this->menuPedidoCorrente();
    }

    public function menuPedidoCorrente($idRestaurante = null, $idPedido = null){

        if(Registry::contains('parametros')) {
            $parametros = Registry::get('parametros');
            if($idRestaurante ==null)
            $idRestaurante = $parametros['id_restaurante'];
            if($idPedido==null)
            $idPedido = $parametros['id_pedido'];
        }
        if($idPedido==null)
            throw new Exception("Pedido deve ter sido inicializado nesse ponto");
        $restaurante= BO_Restaurante::getNomeRestaurante($idRestaurante );
        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type="generic";

        $e = new stdClass();
        $e->title =!strlen($restaurante) ? "Restaurante" : $restaurante;
        $strResumo = $this->getResumoProdutosDoPedido($idPedido);
        if(strlen($strResumo))
            $e->subtitle=$strResumo;
        else $e->subtitle= "Fa�a seu pedido, escolha pratos e bebidas pelo menu abaixo...";

//        $e->default_action = new stdClass();
//        $e->default_action->type="postback";
//        $e->default_action->payload="Cliente@BO_Produto::verProdutosDoRestaurante?id_pedido=".$idPedido;

//        $ret->message->attachment->payload->template_type='button';
//        $ret->message->attachment->payload->text= !strlen($restaurante) ? "Restaurante" : $restaurante;
//        $ret->message->attachment->payload->text .= $strResumo;
        $bs=array();

        $b1=new stdClass();
        $b1->title="Concluir";
        $b1->type="postback";
        $b1->payload="Cliente@BO_Pedido::verResumoPedido?id_pedido=".$idPedido;
        $bs[count($bs)] = $b1;


        $b4=new stdClass();
        $b4->title="Pedir pratos";
        $b4->type="postback";
        $b4->payload="Cliente@BO_Pedido::verProdutosDoRestaurante?id_pedido=".$idPedido
            ."&id_tipo_produto=".BO_Tipo_produto::PRATO;
        $bs[count($bs)] = $b4;

        $b2=new stdClass();
        $b2->title="Pedir bebidas";
        $b2->type="postback";
        $b2->payload="Cliente@BO_Produto::verProdutosDoRestaurante?id_pedido=".$idPedido
            ."&id_tipo_produto=".BO_Tipo_produto::BEBIDA;
        $bs[count($bs)] = $b2;

        $b5=new stdClass();
        $b5->title="Cancelar pedido";
        $b5->type="postback";
        $b5->payload="Cliente@BO_Produto::cancelarPedido?id_pedido=".$idPedido
            ."&id_tipo_produto=".BO_Tipo_produto::BEBIDA;
        $bs[count($bs)] = $b5;

        $e->buttons = $bs;

        $ret->message->attachment->payload->elements = array($e);
//        $ret->message->attachment->payload->buttons=$bs;

        return Helper::jsonEncode($ret);
    }

    public function getResumoProdutosDoPedido($idPedido){
        $q = "SELECT json_produtos FROM pedido WHERE id=$idPedido";
        $db=Registry::get('Database');
        $db->query($q);
        $jsonProdutos = $db->getPrimeiraTuplaDoResultSet(0);
        $strResumo = "";
        if(strlen($jsonProdutos)){
            $objProdutos = json_decode($jsonProdutos);
            $idsProdutos= array_keys(get_object_vars($objProdutos));
            HelperLog::logInfo('teste2: '.print_r($idsProdutos, true));
            $strIn= Helper::arrayToString($idsProdutos);
            $q = "SELECT id, nome, preco FROM produto WHERE id in ($strIn)";
            $db->query($q);
            $ms= Helper::getResultSetToMatriz($db->result);

            $precoTotal=0;
            for($i = 0 ; $i < count($ms); $i++){
                $m=$ms[$i];
                $idProduto=$m['id'];
                $qtd=$objProdutos->$idProduto;
                $preco=floatval($m['preco']);
                $precoTotal+=$preco*$qtd;
                if(strlen($strResumo))$strResumo.=". ";
                $strResumo .= $qtd." - ".Helper::substring( $m['nome'], 10);
            }
        }
        return $strResumo;
    }



    public function getProdutosDoPedido($idPedido){
        $q = "SELECT json_produtos FROM pedido WHERE id=$idPedido";
        $db=Registry::get('Database');
        $db->query($q);
        $jsonProdutos = $db->getPrimeiraTuplaDoResultSet(0);
        $strResumo = "";

        if(strlen($jsonProdutos)){
            $ret=new stdClass();
            $objProdutos = json_decode($jsonProdutos);
            $idsProdutos= array_keys(get_object_vars($objProdutos));
            //HelperLog::logInfo('teste2: '.print_r($idsProdutos, true));
            $strIn= Helper::arrayToString($idsProdutos);
            $q = "SELECT id, nome, preco FROM produto WHERE id in ($strIn)";
            $db->query($q);
            $ms= Helper::getResultSetToMatrizObj($db->result);

            $precoTotal=0;
            for($i = 0 ; $i < count($ms); $i++){
                $m=$ms[$i];
                $idProduto=$m->id;
                $qtd=$objProdutos->$idProduto;
                $preco=floatval($m->preco);
                $precoTotal+=$preco*$qtd;
                if(strlen($strResumo))$strResumo.=". ";
                $strResumo .= $qtd." - ".Helper::substring( $m->nome, 10);
                $m->qtd=$qtd;
                $ms[$i]=$m;
            }
            HelperLog::logInfo('teste4: '.print_r($ms, true));
            $ret->produtos=$ms;
            $ret->precoTotal=$precoTotal;
            return $ret;
        } else return null;
    }



    private function criarPedido($idRestaurante){
        $jsonIteracoes = array();
        $jsonIteracoes[0]= new stdClass();
        $jsonIteracoes[0]->texto = Helper::getDiaEHoraAtual()." come�ou a fazer o pedido...";
        $jsonIteracoes[0]->datetime = Helper::getDiaEHoraEMilisegundoAtual();

        $db = Registry::get('Database');
        $db->query("INSERT INTO pedido (estado_pedido_id_INT, cliente_id_INT, restaurante_id_INT, json_iteracoes)
          VALUES (".BO_Estado_pedido::REALIZANDO
            .", ".Registry::get('idEntidade')
            .", ".$idRestaurante
            .", '".json_encode($jsonIteracoes)."')");

        $idPedido= $db->getLastInsertId();
        return $idPedido;
    }

    public function verProdutosDoRestaurante($idPedido = null){
        //chamada vinda do cliente
        $parametros=Registry::get('parametros');
        //Caso a a��o venha do primeiro fluxo
        //Clica ver pratos
        //pega a coordenada
        //mostra os restaurnates da proximidade
        //atinge esse ponto do codigo criando o pedido relativo ao restaurante selecionado
        if($idPedido == null){
            if(isset($parametros['id_pedido']) && strlen($parametros['id_pedido'])){
                $idPedido=$parametros['id_pedido'];
            } else{
                $idRestaurante = $parametros['id_restaurante'];
                $idPedido = $this->criarPedido($idRestaurante);
            }
        } else {
            //caso ja esteja em andamento um dado
        }
        if($idRestaurante==null)
            $idRestaurante = BO_Pedido::getIdRestaurante($idPedido);


        //HelperLog::logInfo('ENTROU AQUI!!!');
        $limiteInicial = 0;
        if(isset($parametros['limite'])){
            $limiteInicial = $parametros['limite'];
        }
        $sigla= BO_Frequencia::getSigla(Helper::getDiaDaSemana());

        $idTipoProduto = BO_Tipo_produto::PRATO;
        if($idTipoProduto == null)
            $idTipoProduto=$parametros['id_tipo_produto'];

        $q = "SELECT id, nome, preco
            FROM produto 
            WHERE tipo_produto_id_INT = ".$idTipoProduto
            ." AND restaurante_id_INT = ".$idRestaurante
            ." AND {$sigla}_BOOLEAN = 1 "
            ." ORDER BY nome"
            ." LIMIT {$limiteInicial}, ".BO_Facebook::LIMITE_LISTA
        ;

        $get = "&id_restaurante=$idRestaurante&id_pedido=$idPedido";
        $db = Registry::get('Database');
        $db->query($q);

        $pratos = Helper::getResultSetToMatriz($db->result);
        if(!count($pratos))
            return BO_Facebook::getJsonFacebook('N�o existem mais produtos');

        $ret = new stdClass();
        $ret->recipient = new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message = new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='list';
        $ret->message->attachment->payload->top_element_style='compact';

        $ps=array();
        if(count($pratos) > 0){
            if(count($pratos)>1){
                for($i = 0 ;
                    $i < count($pratos) && $i<BO_Facebook::LIMITE_LISTA;
                    $i++, $limiteInicial++){

                    $p =new stdClass();
                    $p->title = $pratos[$i]['nome'];
                    $p->subtitle= "R$ ".$pratos[$i]['preco'];

                    $bs=array();

                    $b=new stdClass();
                    $b->title="Quero esse";
                    $b->type="postback";
                    $b->payload="Cliente@BO_Pedido::escolherQuantidadePrato?"
                        . "id_produto=".$pratos[$i]['id']
                        . $get;
                    $bs[count($bs)] = $b;

                    $p->buttons=$bs;
                    $ps[count($ps)] =$p;
                }
                $ret->message->attachment->payload->elements = $ps;
                $menu = array();
                $b7=new stdClass();
                $b7->title="Ver mais";
                $b7->type="postback";
                $b7->payload="Cliente@BO_Pedido::verPratos?$limiteInicial";
                $menu[count($menu)] = $b7;

                $ret->message->attachment->payload->buttons=$menu;
            } else{
                $prato = $pratos[0];
                $ret->message->attachment->type='template';
                $ret->message->attachment->payload=new stdClass();
                $ret->message->attachment->payload->template_type='button';
                $ret->message->attachment->payload->text = "Prato do dia: ".$prato['nome'];
                $ret->message->attachment->payload->text .= ". Pre�o: R$ ".$prato['preco'].".";
                $b=new stdClass();
                $b->title="Quero esse";
                $b->type="postback";
                $b->payload="Cliente@BO_Pedido::escolherQuantidadePrato?"
                    ."id_produto=".$prato['id']
                    . $get;

                $ret->message->attachment->payload->buttons[0]=$b;

            }
        }


        return Helper::jsonEncode($ret);
    }

    public function getUltimosEnderecosDeEntrega($title = "Qual o endere�o de entrega do seu novo pedido?")
    {

        $idPedido = null;
        $q = "SELECT distinct id, lat_FLOAT lat, lng_FLOAT lng, endereco
            FROM pedido 
            WHERE cliente_id_INT = " . Registry::get('idEntidade')
            . " AND lat_FLOAT is not null"
            . " ORDER BY id DESC"
            . " LIMIT 0, " . BO_Facebook::LIMITE_LISTA;

        //$get = "&id_restaurante=$idRestaurante&id_pedido=$idPedido";
        $db = Registry::get('Database');
        $db->query($q);

        $ultimosEnderecosEntrega = Helper::getResultSetToMatriz($db->result);

        BO_Interacao::registraInteracao("Cliente@BO_Cliente::setEnderecoDeEntrega");

        $ret = new stdClass();
        $ret->recipient = new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message = new stdClass();
        $ret->message->text = $title;
        $ret->message->quick_replies=array();
        $ret->message->quick_replies[0]=new stdClass();
        $ret->message->quick_replies[0]->content_type="text";
        $ret->message->quick_replies[0]->title="Minha posi��o";
        $ret->message->quick_replies[0]->payload="Cliente@BO_Cliente::setEnderecoDeEntrega";

        $ret->message->quick_replies[1]=new stdClass();
        $ret->message->quick_replies[1]->content_type="location";

        if (isset($ultimosEnderecosEntrega)) {
            for($i = 0 ; $i < count($ultimosEnderecosEntrega); $i++) {
                $res = $ultimosEnderecosEntrega[$i];
                $index=$i + 2;
                $ret->message->quick_replies[$index]=new stdClass();
                $ret->message->quick_replies[$index]->content_type="text";
                $ret->message->quick_replies[$index]->title=$res['endereco'];
                $ret->message->quick_replies[$index]->payload="Cliente@BO_Pedido::setEnderecoEntregaAPartirDoPedido?".$res['id'];
//                $ret->message->quick_replies[$index]->image_url="https://maps.googleapis.com/maps/api/staticmap?size=764x400&center="
//                    .$res['lat']. ',' .$res['lng']. '&zoom=25&markers=' . $res['lat'].','.$res['lng'];
            }
        }


        return Helper::jsonEncode($ret);
    }

    public function setEnderecoEntregaAPartirDoPedido()
    {
        $idPedido = Registry::get('idAcao');

        $db=Registry::get('Database');

        $db->query("SELECT lat_FLOAT lat, lng_FLOAT lng, endereco, json_endereco_entrega  FROM pedido WHERE id = $idPedido");
        $m= Helper::getResultSetToMatriz($db->result);
        $res= $m[0];
        $db->query("UPDATE cliente "
            ." SET  lat_FLOAT=".Helper::formatarFloatParaComandoSQL( $res['lat'])
            .", lng_FLOAT=".Helper::formatarFloatParaComandoSQL( $res['lng'])
            .", endereco=".$db->formatarDados( $res['endereco'])
            .", json_endereco_entrega=".$db->formatarDados( $res['json_endereco_entrega'])
            ." WHERE id=".Registry::get('idEntidade'));

        $boCliente = new BO_Cliente();
        return $boCliente->escolherRestauranteProximoAMim();
    }


    public function verUltimoPedidoEmAbertoSeExistente(){

        $q = "SELECT distinct p.id, p.lat_FLOAT lat, p.lng_FLOAT lng, p.endereco, r.nome restaurante
            FROM pedido p join restaurante r ON p.restaurante_id_INT = r.id 
            WHERE p.cliente_id_INT = " . Registry::get('idEntidade')
            . " AND p.estado_pedido_id_INT = ".BO_Estado_pedido::REALIZANDO
            . " ORDER BY p.id DESC"
            . " LIMIT 0, 1" ;

        //$get = "&id_restaurante=$idRestaurante&id_pedido=$idPedido";
        $db = Registry::get('Database');
        $db->query($q);

        $m=Helper::getResultSetToMatriz($db->result);

        if(!count($m)) {
            return null;
        }

        $p = $m[0];

        $ret = new stdClass();
        $ret->recipient = new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->text = "Voc� tem um pedido em aberto no restaurante "
            .ucfirst( $p['restaurante']).", deseja continuar?";
        $ret->message->quick_replies=array();
        $ret->message->quick_replies[0]=new stdClass();
        $ret->message->quick_replies[0]->content_type="text";
        $ret->message->quick_replies[0]->title="N�o";
        $ret->message->quick_replies[0]->payload="Cliente@BO_Pedido::cancelarPedido?id_pedido".$p['id'];

        $ret->message->quick_replies[1]=new stdClass();
        $ret->message->quick_replies[1]->content_type="text";
        $ret->message->quick_replies[1]->title="Sim";
        $ret->message->quick_replies[1]->payload="Cliente@BO_Pedido::prosseguirPedido?".$p['id'];

        return Helper::jsonEncode($ret);
    }

    private function appendIteracaoAoPedido($idPedido, $idEstadoPedido){
        $db = Registry::get('Database');
        $db->query("SELECT json_iteracoes FROM pedido WHERE id = ".$idPedido);
        $jsonIteracoes =  $db->getPrimeiraTuplaDoResultSet(0);
        $objIteracoes = json_decode($jsonIteracoes);
        $objIteracoes = $objIteracoes == null? new stdClass(): $objIteracoes;


        $obj = new stdClass();
        $obj->title = "Pedido ".BO_Estado_pedido::getDescricao($idEstadoPedido)." �s ".Helper::getDiaEHoraAtual();
        $obj->datetime = Helper::getDiaEHoraEMilisegundoAtual();
        $obj->timezone = Helper::getTimezoneOffsetSec();
        $objIteracoes->iteracoes[count($objIteracoes->iteracoes)]=$obj;
        $json=Helper::jsonEncode($objIteracoes);

        $db->query("UPDATE pedido
 SET estado_pedido_id_INT = ".$idEstadoPedido
            .", json_iteracoes = ".$db->formatarDados($json)
            ." WHERE id = ".$idPedido);
    }

    public function cancelarPedido(){
        $idPedido = Registry::get('parametros')['id_pedido'];

        $this->appendIteracaoAoPedido($idPedido, BO_Estado_pedido::CANCELADO_PELO_CLIENTE);

        //ir� carregar os �ltimos endere�os de entrega para o cliente decidir sua localiza��o
        //de entrega para o novo pedido
        $boPedido =new BO_Pedido();
        return $boPedido->getUltimosEnderecosDeEntrega();
    }




    public function prosseguirPedido(){
        $idPedido = Registry::get('idAcao');
        $db = Registry::get('Database');
        $db->query("SELECT restaurante_id_INT FROM pedido WHERE id = $idPedido");
        $idRestaurante= $db->getPrimeiraTuplaDoResultSet(0);
        return $this->menuPedidoCorrente($idRestaurante, $idPedido);
    }

    public function verResumoPedido(){
        $idPedido = Registry::get('parametros')['id_pedido'];

        $restaurante= BO_Pedido::getNomeRestaurante($idPedido );
        $ret = new stdClass();
        $ret->recipient = new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message= new stdClass();

        $ret->message->attachment = new stdClass();
        $ret->message->attachment->type="template";
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type = "receipt";
        $ret->message->attachment->payload->recipient_name=$restaurante;
        $ret->message->attachment->payload->order_number = $idPedido;
        $ret->message->attachment->payload->currency="BRL";
        $ret->message->attachment->payload->payment_method="dinheiro";

        $es=array();
        $objProdutos= $this->getProdutosDoPedido($idPedido);
        //se nao houver pedidos retorna o menu
        if($objProdutos == null){
            return BO_Pedido::menuPedidoCorrente();
        }
        for($i = 0; $i < count($objProdutos->produtos); $i++){
            $e=new stdClass();
            $p= $objProdutos->produtos[$i];
            $e->title=$p->nome;
            $e->quantity=$p->qtd;
            $e->price=$p->preco;
            $es[count($es)]=$e;
        }
        $ret->message->attachment->payload->elements = $es;
        $ret->message->attachment->payload->summary = new stdClass();
        $ret->message->attachment->payload->summary->total_cost = $objProdutos->precoTotal;

        $idEstadoPedido = BO_Pedido::getIdEstadoPedido($idPedido);
        if($idEstadoPedido== BO_Estado_pedido::REALIZANDO){
            $ret->message->quick_replies=array();
            $ret->message->quick_replies[0]=new stdClass();
            $ret->message->quick_replies[0]->content_type="text";
            $ret->message->quick_replies[0]->title="Concluir pedido";
            $ret->message->quick_replies[0]->payload="Cliente@BO_Pedido::concluirPedido?id_pedido=$idPedido";
        }


        return Helper::jsonEncode($ret);

    }


    public function concluirPedido(){
        $idPedido = Registry::get('parametros')['id_pedido'];

        BO_Pedido::appendIteracaoAoPedido($idPedido, BO_Estado_pedido::APROVADO);

        return BO_Facebook::getJsonFacebook("Obrigado! Seu pedido foi conclu�do com sucesso!");
    }


    public function verDetalhesPedido(){
        $idPedido = Registry::get('parametros')['id_pedido'];
        $q="SELECT estado_pedido_id_INT idEstadoPedido, json_produtos jsonProdutos, json_iteracoes jsonIteracoes 
FROM pedido 
WHERE id = $idPedido";
        $db = Registry::get('Database');
        $db->query($q);
        $res=  Helper::getResultSetToMatriz($db->result);
        $r = $res[0];
        //Lembre que o json_produtos conter� os valores dos produtos apos a conclusao pelo cliente do pedido
        //TODO reajustar o proceidmento de fechar o pedido para incluir esses valores no json_produtos
        //pois o json_produtos contem apenas o id do produto selecionado e a qtd durante a realizacao do pedido
        $objRecibo = $this->getProdutosDoPedido($idPedido);
        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');

        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type = 'generic';
        $es= array();
        $e = new stdClass();
        $e->title = "Pedido $idPedido - R$ ".$objRecibo->precoTotal;

        $e->subtitle = BO_Estado_pedido::getDescricao($r['idEstadoPedido'])
            .". ".$this->formatarJsonIteracoesParaExibicao($r['jsonIteracoes']);
        $bs = array();
        $b = new stdClass();
        $b->title = "Recibo";
        $b->type="postback";
        $b->payload="Cliente@BO_Pedido::verResumoPedido?id_pedido=$idPedido";
        $bs[count($bs)]=$b;

        $b1 = new stdClass();
        $b1->title = "Contato";
        $b1->type="postback";
        $b1->payload="Cliente@BO_Restaurante::verContato?id_restaurante=$idPedido";
        $bs[count($bs)]=$b1;

        $e->buttons = $bs;
        
        $es[count($es)] = $e;
        $ret->message->attachment->payload->elements = $es;

        return Helper::jsonEncode($ret);
    }

    private function formatarJsonIteracoesParaExibicao($json){
        $its = json_decode($json);
        $desc = "";
        HelperLog::logInfo("Iteracoes: ".print_r($its, true));
        for($i = count($its->iteracoes) - 1 ; $i >=0; $i--){
            $it= $its->iteracoes[$i];
            if(strlen($desc)) $desc.= ". ";
            $desc .= $it->title;
        }
        return $desc;
//        $obj->title = "Pedido ".BO_Estado_pedido::getDescricao($idEstadoPedido)." �s ".Helper::getDiaEHoraAtual();
//        $obj->datetime = Helper::getDiaEHoraEMilisegundoAtual();
//        $obj->timezone = Helper::getTimezoneOffsetSec();
//        $objIteracoes->iteracoes[count($objIteracoes->iteracoes)]=$obj;
//        $json=Helper::jsonEncode($objIteracoes);
    }

}
