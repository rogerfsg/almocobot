<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 16/04/2018
 * Time: 12:45
 */
class BO_Tipo_produto
{
    const PRATO= 1;
    const  BEBIDA = 2;

    public static function getLabel($id, $plural=false){
        switch ($id){
            case BO_Tipo_produto::PRATO:
                return $plural?"pratos": "prato";
            case BO_Tipo_produto::BEBIDA:
                return $plural?"bebidas": "bebida";
        }
        return null;
    }
}