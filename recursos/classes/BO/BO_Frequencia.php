<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 16/04/2018
 * Time: 12:45
 */
class BO_Frequencia
{
    const TODOS_OS_DIAS=-1;
    const SEGUNDA_A_SEXTA=-2;
    const TODO_DOMINGO=0;
    const TODA_SEGUNDA=1;
    const TODA_TERCA=2;
    const TODA_QUARTA=3;
    const TODA_QUINTA=4;
    const TODA_SEXTA=5;
    const TODO_SABADO=6;

    const LABEL_SEGUNDA_A_SEXTA="Segunda � Sexta";
    const LABEL_DOMINGO= "Dom";
    const LABEL_SEGUNDA= "Seg";
    const LABEL_TERCA="Ter";
    const LABEL_QUARTA="Qua";
    const LABEL_QUINTA="Qui";
    const LABEL_SEXTA="Sex";
    const LABEL_SABADO="Sab";

    const aliasDomingo = array("domingo", "dom", "d", "domin", "do");
    const aliasSegunda = array("segunda", "seg", "se", "segun", "segund","segunda-feira", "segunda feira");
    const aliasTerca = array("terca", "ter�a", "t", "te", "ter", "terca-feira", "ter�a-feira", "terca feira", "terca-feira");
    const aliasQuarta = array("quarta", "quar", "qua", "quarta", "quarta-feira", "quarta feira");
    const aliasQuinta= array("quinta", "quin", "qui", "q", "quinta-feira", "quinta feira");
    const aliasSexta= array("sexta", "sex", "sext", "sexta-feira", "sexta feira");
    const aliasSabado= array("sabado", "sab", "saba", "s�bado", "s�b");
    const diasSemanaPorAlias = array(
        BO_Frequencia::TODO_DOMINGO => BO_Frequencia::aliasDomingo,
        BO_Frequencia::TODA_SEGUNDA => BO_Frequencia::aliasSegunda,
        BO_Frequencia::TODA_TERCA => BO_Frequencia::aliasTerca,
        BO_Frequencia::TODA_QUARTA=> BO_Frequencia::aliasQuarta,
        BO_Frequencia::TODA_QUINTA=> BO_Frequencia::aliasQuinta,
        BO_Frequencia::TODA_SEXTA=> BO_Frequencia::aliasSexta,
        BO_Frequencia::TODO_SABADO=> BO_Frequencia::aliasSabado,
    );
    public function identificarDiasDaSemana($token){
        $token = Helper::strtolowerlatin1($token);
        $dias = array();
        if(substr_count($token, "todo") > 0){
            return BO_Frequencia::getIdsDomingoASabado();
        } else if(substr_count($token, "nenhum") > 0){
            return array();
        }
        foreach (BO_Frequencia::diasSemanaPorAlias as $k => $v){
            foreach ($v as $alias){
                if(substr_count($token, $alias) > 0){

                    $dias [count($dias )]=$k;
                    break;
                }
            }
        }
        return $dias;
    }

    const itens = array(
        BO_Frequencia::SEGUNDA_A_SEXTA,
        BO_Frequencia::TODO_DOMINGO,
        BO_Frequencia::TODA_SEGUNDA,
        BO_Frequencia::TODA_TERCA,
        BO_Frequencia::TODA_QUARTA,
        BO_Frequencia::TODA_QUINTA,
        BO_Frequencia::TODA_SEXTA,
        BO_Frequencia::TODO_SABADO,
        );

    const diaDaSemanaPorAtributo = array(
        BO_Frequencia::TODO_DOMINGO => "dom_BOOLEAN",
        BO_Frequencia::TODA_SEGUNDA=> "seg_BOOLEAN",
        BO_Frequencia::TODA_TERCA=> "ter_BOOLEAN",
        BO_Frequencia::TODA_QUARTA=> "qua_BOOLEAN",
        BO_Frequencia::TODA_QUINTA=> "qui_BOOLEAN",
        BO_Frequencia::TODA_SEXTA=> "sex_BOOLEAN",
        BO_Frequencia::TODO_SABADO=> "sab_BOOLEAN",
    );

    const diaDaSemanaPorSigla = array(
        BO_Frequencia::TODO_DOMINGO => "dom",
        BO_Frequencia::TODA_SEGUNDA=> "seg",
        BO_Frequencia::TODA_TERCA=> "ter",
        BO_Frequencia::TODA_QUARTA=> "qua",
        BO_Frequencia::TODA_QUINTA=> "qui",
        BO_Frequencia::TODA_SEXTA=> "sex",
        BO_Frequencia::TODO_SABADO=> "sab",
    );
    public static function getSigla($idDiaSemana){
        return BO_Frequencia::diaDaSemanaPorSigla[$idDiaSemana];
    }
    public static function getDiaDaSemanaPorNomeAtributo(){
        return BO_Frequencia::diaDaSemanaPorAtributo;
    }

    public static function getIdsFrequencia(){
        return BO_Frequencia::itens;
    }
    public static function getIdsSegundaASexta()
    {
        $ids = array(
            BO_Frequencia::TODA_SEGUNDA,
            BO_Frequencia::TODA_TERCA,
            BO_Frequencia::TODA_QUARTA,
            BO_Frequencia::TODA_QUINTA,
            BO_Frequencia::TODA_SEXTA,
        );
        return $ids;
    }
    public static function getIdsDomingoASabado()
    {
        $ids = array(
            BO_Frequencia::TODO_DOMINGO,
            BO_Frequencia::TODA_SEGUNDA,
            BO_Frequencia::TODA_TERCA,
            BO_Frequencia::TODA_QUARTA,
            BO_Frequencia::TODA_QUINTA,
            BO_Frequencia::TODA_SEXTA,
            BO_Frequencia::TODO_SABADO
        );
        return $ids;
    }
    public static function isDiaDaSemana($id){
        switch ($id){
            case BO_Frequencia::TODO_DOMINGO:
            case BO_Frequencia::TODA_SEGUNDA:
            case BO_Frequencia::TODA_TERCA:
            case BO_Frequencia::TODA_QUARTA:
            case BO_Frequencia::TODA_QUINTA:
            case BO_Frequencia::TODA_SEXTA:
            case BO_Frequencia::TODO_SABADO:
                return true;
        }
        return false;
    }
    public static function getLabel($id){
        switch ($id){
            case BO_Frequencia::SEGUNDA_A_SEXTA:
                return BO_Frequencia::LABEL_SEGUNDA_A_SEXTA;
            case BO_Frequencia::TODO_DOMINGO:
                return BO_Frequencia::LABEL_DOMINGO;
            case BO_Frequencia::TODA_SEGUNDA:
                return BO_Frequencia::LABEL_SEGUNDA;
            case BO_Frequencia::TODA_TERCA:
                return BO_Frequencia::LABEL_TERCA;
            case BO_Frequencia::TODA_QUARTA:
                return BO_Frequencia::LABEL_QUARTA;
            case BO_Frequencia::TODA_QUINTA:
                return BO_Frequencia::LABEL_QUINTA;
            case BO_Frequencia::TODA_SEXTA:
                return BO_Frequencia::LABEL_SEXTA;
            case BO_Frequencia::TODO_SABADO:
                return BO_Frequencia::LABEL_SABADO;
        }
        return null;
    }

    public static function getLabelCompleta($id){
        switch ($id){
            case BO_Frequencia::SEGUNDA_A_SEXTA:
                return "segunda a sexta";
            case BO_Frequencia::TODO_DOMINGO:
                return "domingo";
            case BO_Frequencia::TODA_SEGUNDA:
                return "segunda";
            case BO_Frequencia::TODA_TERCA:
                return "ter�a";
            case BO_Frequencia::TODA_QUARTA:
                return "quarta";
            case BO_Frequencia::TODA_QUINTA:
                return "quinta";
            case BO_Frequencia::TODA_SEXTA:
                return "sexta";
            case BO_Frequencia::TODO_SABADO:
                return "s�bado";
        }
        return null;
    }


    public static function getLabels(){
        return array(
            "Segunda � Sexta",
            "Dom",
            "Seg",
            "Ter",
            "Qua",
            "Qui",
            "Sex",
            "Sab",
        );
    }

}