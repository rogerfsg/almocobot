<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 17/04/2018
 * Time: 05:40
 */
class BO_Interacao
{
    public static function factory(){
        return new BO_Interacao();
    }
    public static function carregaUltimaInteracao()
    {
        $senderId = Registry::get('senderId');
        $db=Registry::get('Database');
        $db->query("SELECT
                classe,
                acao,
                id_entidade_INT,
                sender_id,
                cadastro_DATETIME,
                json,
                ator_id_INT,
                id_acao_INT,
                parametros
  FROM interacao WHERE sender_id = $senderId
  ORDER BY id DESC 
  LIMIT 0,1");

        $obj = Helper::getPrimeiroRegistroDoResultSetAsObject($db->result);
        if($obj != null) {
            Registry::add($obj->classe,'classe');
            Registry::add($obj->acao,'acao');
            Registry::add($obj->id_entidade_INT,'idEntidade');
            Registry::add($obj->sender_id,'senderId');
            Registry::add(json_decode( $obj->json),'objJsonInteracao');
            Registry::add($obj->ator_id_INT,'idAtor');
            Registry::add($obj->id_acao_INT,"idAcao");


            BO_Interacao::formatarParametros($obj->parametros);
            return $obj;
        }
        else return null;
    }

    public static function formatarParametros($strParametros){
        Registry::add($strParametros, 'strParametros');
        HelperLog::logInfo('Parametros: '.$strParametros);
        if(is_numeric($strParametros)){
            $id = $strParametros;
            $m['id']=$id;
        } else {
            $p2=Helper::explode('[&]', $strParametros);
            if($p2 != null) $parametros=$p2;
            else $parametros=array($strParametros);

            for($i = 0 ; $i < count($parametros); $i++){
                $cv=Helper::explode('[=]', $parametros[$i]);
//                    HelperLog::logInfo('=: '.print_r($cv,true));
                $m[$cv[0]]=$cv[1];
            }
        }
        Registry::add($m, 'parametros');
        HelperLog::logInfo('Parametros: '.print_r($m,true));

    }

    public static function getPenultimaInteracao()
    {
        $senderId = Registry::get('senderId');
        $db=Registry::get('Database');
        $db->query("SELECT
                classe,
                acao,
                id_entidade_INT,
                sender_id,
                cadastro_DATETIME,
                json,
                ator_id_INT,
                id_acao_INT
  FROM interacao WHERE sender_id = $senderId
  ORDER BY id DESC 
  LIMIT 1,1");

        $obj = Helper::getPrimeiroRegistroDoResultSetAsObject($db->result);
        if($obj != null) {
            return $obj;
        }
        else return null;
    }
    public static function loadEntidade(){

        if(Registry::contains("idEntidade"))
            return Registry::get("idEntidade");

        $idAtor = Registry::get('idAtor');

        $senderId = Registry::get('senderId');
        $idEntidade = null;
        switch ($idAtor){
            case BO_Ator::RESTAURANTE:
                $idEntidade = BO_Restaurante::getIdRestaurante($senderId);
                break;
            case BO_Ator::CLIENTE:
                $idEntidade = BO_Cliente::getIdCliente($senderId);
                break;
        }

        Registry::add($idEntidade,"idEntidade");


        return $idEntidade;

    }
    public static function registraInteracao($payload, $json = null){

        $v= Helper::explode('[@]', $payload);

        //N�o est� no padr�o pr� determinado
        //ator@classe::acao
        //Restaurante@BO_Produto::verProdutos
        if(count($v) != 2) return false;

        HelperLog::logInfo("Depois do explode: ");
        HelperLog::logInfo(print_r($v, true));
        Registry::add($v[0] == 'Restaurante' ? BO_Ator::RESTAURANTE : BO_Ator::CLIENTE, 'idAtor');

        $acoes= Helper::explode('[::]', $v[1]);
        if(count($acoes) != 2) return false;
        HelperLog::logInfo(print_r($acoes, true));
        $classe = $acoes[0];
        $acao =null;
        $id=null;
        $acaoPorId = Helper::explode('[\?]', $acoes[1]);
        $strParametros=null;
//        HelperLog::logInfo('asdf?: '.print_r($acaoPorId,true));
        $m=array();

        if(count($acaoPorId) > 1){
            $strParametros=$acaoPorId[1];
            $acao=$acaoPorId[0];
            if(is_numeric($acaoPorId[1])){
                $id = $acaoPorId[1];
                $m['id']=$id;
            } else {
                $p2=Helper::explode('[&]', $acaoPorId[1]);
                if($p2 != null) $parametros=$p2;
                else $parametros=array($acaoPorId[1]);

                for($i = 0 ; $i < count($parametros); $i++){
                    $cv=Helper::explode('[=]', $parametros[$i]);
//                    HelperLog::logInfo('=: '.print_r($cv,true));
                    $m[$cv[0]]=$cv[1];
                }
            }
        }else {
            $acao = $acoes[1];
        }
        HelperLog::logInfo('Parameotros: '.print_r($m,true));
        Registry::add($m, 'parametros');
        Registry::add($strParametros, 'strParametros');

        Registry::add($acoes[0], 'classe');
        Registry::add($acao, 'acao');
        if(is_numeric($id)) Registry::add($id, 'idAcao');
        else Registry::add(null, 'idAcao');

        $idAtor = Registry::get('idAtor');
        $senderId = Registry::get('senderId');


        $idEntidade = BO_Interacao::loadEntidade();
        if($idEntidade==null){
            $idEntidade = BO_Interacao::inserirIdEntidade();
        }
        $strParametros = Registry::contains('strParametros') ? Registry::get('strParametros') : null;
        //$json = json_encode($obj);
        $db = Registry::get('Database');

        $db->query("INSERT interacao (classe, acao, id_acao_INT, sender_id, ator_id_INT, cadastro_DATETIME, id_entidade_INT, json, parametros) VALUES ("
            .$db->formatarDados($classe).", "
            .$db->formatarDados($acao).", "
            .$db->formatarDados($id).", "
            .$db->formatarDados($senderId).", "
            .$idAtor.", "
            ."'".Helper::getDiaEHoraAtualSQL()."',"
            ."$idEntidade, "
            .$db->formatarDados($json)." ,
            ".$db->formatarDados($strParametros)."
            )");

        return $acao;
    }

    public static function inserirIdEntidade(){

        $idAtor = Registry::get('idAtor');

        $senderId = Registry::get('senderId');
        $idEntidade = null;
        switch ($idAtor){
            case BO_Ator::RESTAURANTE:
                $idEntidade = BO_Restaurante::inserirRestaurante($senderId);
                break;
            case BO_Ator::CLIENTE:
                $idEntidade = BO_Cliente::inserir($senderId);
                break;
        }

        Registry::add($idEntidade,"idEntidade");
        return $idEntidade;
    }
}