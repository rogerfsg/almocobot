<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 16/04/2018
 * Time: 12:45
 */
class BO_Produto
{
    public static function factory(){
        return new BO_Produto();
    }
    public function editarNome(){
        $db=new Database();
        Registry::add('Database', $db);

        $nome = Helper::POSTGET("nome");
        $ultimaEdicao = BO_Interacao::carregaUltimaInteracao();

        $msg = $db->queryMensagem("UPDATE produto SET nome = ".$db->formatarDados($nome) ." WHERE restaurante_id_INT= ".$ultimaEdicao["id_entidade"]);

        if(Interface_mensagem::checkErro($msg) && !$msg->erroSqlChaveDuplicada())
            return $msg;
        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
    }

    public function cadastrarBebida(){
//        $db = new Database();
//        Registry::add($db , 'Database');
//        $idRestaurante = Registry::get('idEntidade');

        BO_Interacao::registraInteracao("Restaurante@BO_Produto::cadastrarNomeProduto?".BO_Tipo_produto::BEBIDA);

        return BO_Facebook::getJsonFacebook("Qual o nome da bebida?");
    }
    public function cadastrarPrato(){
//        $db = new Database();
//        Registry::add($db , 'Database');
//        $idRestaurante = Registry::get('idEntidade');

        BO_Interacao::registraInteracao("Restaurante@BO_Produto::cadastrarNomeProduto?".BO_Tipo_produto::PRATO);

        return BO_Facebook::getJsonFacebook("Qual o nome do prato?");
    }
    public function cadastrarNomeProduto(){
        $db = new Database();
        Registry::add($db , 'Database');
        $nome = Registry::get('messageFacebook');
        $idRestaurante = Registry::get('idEntidade');


        $msg = $db->queryMensagem(
            "INSERT produto (nome, restaurante_id_INT, tipo_produto_id_INT)
            VALUES (".$db->formatarDados($nome) .", $idRestaurante, ".Registry::get('idAcao').")");

        if(Interface_mensagem::checkErro($msg) && !$msg->erroSqlChaveDuplicada())
            return BO_Facebook::getJsonFacebookDaMensagem($msg);

        $idProduto =$db->getLastInsertId();

        BO_Interacao::registraInteracao("Restaurante@BO_Produto::cadastrarPreco?$idProduto");

        return BO_Facebook::getJsonFacebook("Qual o pre�o do prato?");
    }



    public function cadastrarPreco(){

        $db = Registry::get( 'Database');
        //pegar o ultimo id inserido
        $idProduto = Registry::get('idAcao');
        $preco = BO_Facebook::getPrecoDaMensagem();
        if($preco==null) return BO_Facebook::getMensagemInstrucaoPreco();
//        HelperLog::logInfo("Preco 3: $preco");

        $strPreco= $db->formatarDados($preco);
        $db->query(
            "UPDATE produto "
            ." SET preco = ".$strPreco
            ." WHERE id = ".$idProduto);
        $db->query("SELECT nome FROM produto WHERE id=".$idProduto);
        $nome=$db->getPrimeiraTuplaDoResultSet(0);
        $unome=ucfirst($nome);
        if($this->getTipoProduto($idProduto)==BO_Tipo_produto::PRATO){
            return $this->frequenciaDoPrato();
        } else {
            return BO_Produto::getJsonFacebookSucessoCadastro(
                "Sucesso! ".utf8_encode($unome)." por R$ ".$preco,$idProduto );
        }
    }

    public function frequenciaDoPrato(){

        $limiteInicial = 0;
//        if(Registry::contains('idAcao'))
//            $limiteInicial = Registry::get('idAcao');

        BO_Interacao::registraInteracao("Restaurante@BO_Produto::setFrequenciaDoPrato?".Registry::get('idAcao'));
        return BO_Facebook::getJsonFacebook("Quais dias da semana voc� entrega esse prato? Segunda, quarta e sexta?");
    }
    public function setFrequenciaDoPrato(){
        $message = Registry::get('messageFacebook');
        $dias= BO_Frequencia::identificarDiasDaSemana($message);
        HelperLog::logInfo("Dias: ".print_r($dias,true));
        $idProduto = Registry::get('idAcao');
        $strUpdate = "";
        $diaDaSemanaPorNomeAtributo= BO_Frequencia::getDiaDaSemanaPorNomeAtributo();
        $strDiasDaSemana = "";
        foreach($diaDaSemanaPorNomeAtributo as $idDia => $attr){
            if(strlen($strUpdate)){ $strUpdate.= ", "; }
            $res= array_search($idDia, $dias);
            HelperLog::logInfo("array_search($idDia, $dias) = $res");
            if($res ===0 || $res > 0){
                $strUpdate .= " $attr = 1 ";
                if(strlen($strDiasDaSemana  )) $strDiasDaSemana  .=", ";
                $strDiasDaSemana  .= BO_Frequencia::getLabelCompleta($idDia);
            } else {
                $strUpdate .= " $attr = 0 ";
            }
        }
        $db = Registry::get('Database');
        $db->query("UPDATE produto SET $strUpdate WHERE id=$idProduto");
        if(count($dias) > 0) {
            return BO_Facebook::getJsonFacebook("O prato estar� dispon�vel no(s) dia(s): $strDiasDaSemana");
        }
        else
            return BO_Facebook::getJsonFacebook("O prato n�o est� dispon�vel nenhum dia. Nenhum cliente � capaz de pedir." );

    }
    public function getObjJson($idProduto)
    {
        $db=Registry::get('Database');
        $db->query("SELECT json FROM produto WHERE id= $idProduto");
        $jsonRestaurante = $db->getPrimeiraTuplaDoResultSet(0);
        $objJson= json_decode($jsonRestaurante );
        $objJson=$objJson==null?new stdClass():$objJson;
        return $objJson;
    }

    public function setarFrequencia(){
        $limiteInicial = 0;
        if(Registry::contains('idAcao'))
            $limiteInicial = Registry::get('idAcao');

        //$objJson = $this->get

    }

    public function removerUltimoProduto(){
        $objPenultima= BO_Interacao::getPenultimaInteracao();
        if($objPenultima!=null && isset($objPenultima->objJson)){
            $idProduto = $objPenultima->id_acao_INT;
            $db = Registry::get('Database');
            $db->query("DELETE FROM produto WHERE id = ".$idProduto);
            return BO_Facebook::getJsonFacebook('Produto removido!');
        }else {
            return BO_Facebook::getJsonFacebook('Desculpa! Esqueci qual era o prato!');
        }
    }

    public function getDescricaoProduto($idProduto){
        $db = Registry::get('Database');
        $db->query("SELECT nome, preco FROM produto WHERE id = $idProduto" );
        $m= Helper::getResultSetToMatriz($db->result);
        return $m[0]['nome']. ' - R$ '. $m[0]['preco'];
    }
    public function getNomeProduto($idProduto){
        $db = Registry::get('Database');
        $db->query("SELECT nome FROM produto WHERE id = $idProduto" );
        $m= Helper::getResultSetToMatriz($db->result);
        return $m[0]['nome'];
    }

    public function getTipoProduto($idProduto){
        $db = Registry::get('Database');
        $db->query("SELECT tipo_produto_id_INT FROM produto WHERE id = $idProduto" );
        $m= Helper::getResultSetToMatriz($db->result);
        return $m[0]['tipo_produto_id_INT'];
    }
    public function getPrecoProduto($idProduto){
        $db = Registry::get('Database');
        $db->query("SELECT preco FROM produto WHERE id = $idProduto" );
        $m= Helper::getResultSetToMatriz($db->result);
        return "R$ ".$m[0]['preco'];
    }

    public function gerenciarProduto(){
//
        $idProduto = Registry::get('idAcao');

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='button';
        $ret->message->attachment->payload->text= $this->getDescricaoProduto($idProduto);

        $bs=array();

        $b1=new stdClass();
        $b1->title="Editar";
        $b1->type="postback";
        $b1->payload="Restaurante@BO_Produto::editarProduto?".$idProduto;
        $bs[count($bs)] = $b1;


        if($this->getTipoProduto($idProduto) == BO_Tipo_produto::PRATO){
            $b4=new stdClass();
            $b4->title="Dias do prato";
            $b4->type="postback";
            $b4->payload="Restaurante@BO_Produto::diasDoPrato?".$idProduto;
            $bs[count($bs)] = $b4;
        }



        $b2=new stdClass();
        $b2->title="Remover";
        $b2->type="postback";
        $b2->payload="Restaurante@BO_Produto::removerProduto?".$idProduto;
        $bs[count($bs)] = $b2;

        $ret->message->attachment->payload->buttons=$bs;

        return Helper::jsonEncode($ret);
    }



    public function editarProduto(){
//        $db = new Database();
//        Registry::add($db , 'Database');
//        $idRestaurante = Registry::get('idEntidade');
        $idProduto = Registry::get('idAcao');
        BO_Interacao::registraInteracao("Restaurante@BO_Produto::editarNomeProduto?$idProduto");

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='button';
        $ret->message->attachment->payload->text= $this->getNomeProduto($idProduto).". Qual a nova descri��o do prato?";

        $bs=array();

        $b1=new stdClass();
        $b1->title="Sem altera��o";
        $b1->type="postback";
        $b1->payload="Restaurante@BO_Produto::seguirParaEditarPrecoProduto?".$idProduto;
        $bs[count($bs)] = $b1;


        $ret->message->attachment->payload->buttons=$bs;

        return Helper::jsonEncode($ret);
    }

    public function seguirParaEditarPrecoProduto(){
//        $db = new Database();
//        Registry::add($db , 'Database');
//        $idRestaurante = Registry::get('idEntidade');

        $idProduto = Registry::get('idAcao');

        BO_Interacao::registraInteracao("Restaurante@BO_Produto::editarPrecoProduto?$idProduto");

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='button';
        $ret->message->attachment->payload->text= $this->getPrecoProduto($idProduto). ". Qual o novo pre�o?";

        $bs=array();

        $b1=new stdClass();
        $b1->title="Sem altera��o";
        $b1->type="postback";
        $b1->payload="Restaurante@BO_Produto::finalizarEdicaoProduto?".$idProduto;
        $bs[count($bs)] = $b1;


        $ret->message->attachment->payload->buttons=$bs;

        return Helper::jsonEncode($ret);
    }
    public function editarNomeProduto(){
//        $db = new Database();
//        Registry::add($db , 'Database');
//        $idRestaurante = Registry::get('idEntidade');
        $db=Registry::get('Database');
        $idProduto = Registry::get('idAcao');
        $db->query("UPDATE produto SET nome = ".$db->formatarDados(Registry::get('messageFacebook'))." WHERE id=$idProduto");
        BO_Interacao::registraInteracao("Restaurante@BO_Produto::editarPrecoProduto?$idProduto");

        $ret = new stdClass();
        $ret->recipient=new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message=new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='button';
        $ret->message->attachment->payload->text= $this->getPrecoProduto($idProduto). ". Qual o novo pre�o?";

        $bs=array();

        $b1=new stdClass();
        $b1->title="Sem altera��o";
        $b1->type="postback";
        $b1->payload="Restaurante@BO_Produto::finalizarEdicaoProduto?".$idProduto;
        $bs[count($bs)] = $b1;


        $ret->message->attachment->payload->buttons=$bs;

        return Helper::jsonEncode($ret);
    }
    public function removerProduto(){
        $idProduto = Registry::get('idAcao');
        $db = Registry::get('Database');
        $db->query('DELETE FROM produto WHERE id = '.$idProduto);
        return BO_Facebook::getJsonFacebook('Removido!');
    }
    public function finalizarEdicaoProduto(){
        $idProduto = Registry::get('idAcao');
        return BO_Facebook::getJsonFacebook('Sucesso! '.$this->getDescricaoProduto($idProduto));
    }
    public function editarPrecoProduto(){
        $db = Registry::get('Database');
        $idProduto=Registry::get('idAcao');
        $preco= BO_Facebook::getPrecoDaMensagem();
        if($preco==null)return BO_Facebook::getMensagemInstrucaoPreco();

        $db->query("UPDATE produto SET preco = ".$db->formatarDados($preco)." WHERE id=$idProduto");

        return BO_Facebook::getJsonFacebook('Sucesso! '.$this->getDescricaoProduto($idProduto));
    }

    public function verPratos(){
        HelperLog::logInfo("Q:  ");
        $limiteInicial = 0;
        if(Registry::contains('idAcao'))
            $limiteInicial = Registry::get('idAcao');

        $db = Registry::get('Database');
        $q = "SELECT id, nome, preco 
            FROM produto 
            WHERE tipo_produto_id_INT = ".BO_Tipo_produto::PRATO
        ." AND restaurante_id_INT = ".Registry::get('idEntidade')
            ." ORDER BY nome"
            ." LIMIT {$limiteInicial}, ".BO_Facebook::LIMITE_LISTA
        ;

        $db->query($q);

        $pratos = Helper::getResultSetToMatriz($db->result);
        if(!count($pratos))
            return BO_Facebook::getJsonFacebook('N�o existem mais produtos');
//        HelperLog::logInfo("PASSOU...");
//        HelperLog::logInfo(print_r($pratos,true));

        $ret = new stdClass();
        $ret->recipient = new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message = new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='list';
        $ret->message->attachment->payload->top_element_style='compact';

        $ps=array();


        for($i = 0 ; $i < count($pratos) && $i<BO_Facebook::LIMITE_LISTA; $i++, $limiteInicial++){
            $p =new stdClass();
            $p->title = $pratos[$i]['nome'];
            $p->subtitle= "R$ ".$pratos[$i]['preco'];

            $bs=array();

            $b=new stdClass();
            $b->title="Gerenciar produto";
            $b->type="postback";
            $b->payload="Restaurante@BO_Produto::gerenciarProduto?".$pratos[$i]['id'];
            $bs[count($bs)] = $b;

            $p->buttons=$bs;
            $ps[count($ps)] =$p;
        }
        $ret->message->attachment->payload->elements = $ps;
        $menu = array();
        $b7=new stdClass();
        $b7->title="Ver mais";
        $b7->type="postback";
        $b7->payload="Restaurante@BO_Produto::verPratos?$limiteInicial";
        $menu[count($menu)] = $b7;

//        $b6=new stdClass();
//        $b6->title="Cadastrar";
//        $b6->type="postback";
//        $b6->payload="Restaurante@BO_Produto::cadastrarPrato";
//        $menu[count($menu)] = $b6;


        $ret->message->attachment->payload->buttons=$menu;
        return Helper::jsonEncode($ret);
    }


    public function verBebidas(){
        HelperLog::logInfo("Q:  ");
        $limiteInicial = 0;
        if(Registry::contains('idAcao'))
            $limiteInicial = Registry::get('idAcao');

        $db = Registry::get('Database');
        $q = "SELECT id, nome, preco 
            FROM produto 
            WHERE tipo_produto_id_INT = ".BO_Tipo_produto::BEBIDA
            ." AND restaurante_id_INT = ".Registry::get('idEntidade')
            ." ORDER BY nome"
            ." LIMIT {$limiteInicial}, ".BO_Facebook::LIMITE_LISTA
        ;

        $db->query($q);

        $bebidas = Helper::getResultSetToMatriz($db->result);
        if(!count($bebidas))
            return BO_Facebook::getJsonFacebook('N�o existem mais produtos');
//        HelperLog::logInfo("PASSOU...");
        HelperLog::logInfo(print_r($bebidas,true));

        $ret = new stdClass();
        $ret->recipient = new stdClass();
        $ret->recipient->id=Registry::get('senderId');
        $ret->message = new stdClass();
        $ret->message->attachment=new stdClass();
        $ret->message->attachment->type='template';
        $ret->message->attachment->payload=new stdClass();
        $ret->message->attachment->payload->template_type='list';
        $ret->message->attachment->payload->top_element_style='compact';

        $ps=array();
        HelperLog::logInfo(print_r($bebidas, true));

        for($i = 0 ; $i < count($bebidas) && $i<BO_Facebook::LIMITE_LISTA; $i++, $limiteInicial++){
            $p =new stdClass();
            $p->title = $bebidas[$i]['nome'];
            $p->subtitle= "R$ ".$bebidas[$i]['preco'];

            $bs=array();

            $b=new stdClass();
            $b->title="Gerenciar produto";
            $b->type="postback";
            $b->payload="Restaurante@BO_Produto::gerenciarProduto?".$bebidas[$i]['id'];
            $bs[count($bs)] = $b;

            $p->buttons=$bs;
            $ps[count($ps)] =$p;
        }
        $ret->message->attachment->payload->elements = $ps;
        $menu = array();
        $b7=new stdClass();
        $b7->title="Ver mais";
        $b7->type="postback";
        $b7->payload="Restaurante@BO_Produto::verBebidas?$limiteInicial";
        $menu[count($menu)] = $b7;
        $ret->message->attachment->payload->buttons=$menu;
        return Helper::jsonEncode($ret);
    }


    public function getJsonFacebookSucessoCadastro($texto, $idProduto){
        $idTipoProduto= $this->getTipoProduto($idProduto);
        $label= BO_Tipo_produto::getLabel($idTipoProduto,true);
        $jsonData="";
        switch ($idTipoProduto){
            case BO_Tipo_produto::BEBIDA:
                $jsonData = '{
            "messaging_type": "RESPONSE",
            "recipient":{
                "id": "' . Registry::get('senderId') . '"
            }, 
            "message":{
                "attachment":{
                  "type":"template",
                  "payload":{
                    "template_type":"button",
                    "text":"'.utf8_encode($texto).'",
                    "buttons":[
                      {
                        "type":"postback",
                        "title":"Adicionar outra",
                        "payload": "Restaurante@BO_Produto::cadastrarBebida"
                      },
                      {
                        "type":"postback",
                        "title":"Remover '.utf8_encode('�ltima').'",
                        "payload": "Restaurante@BO_Produto::removerUltimoProduto?'.$idProduto.'"
                      },
                      {
                        "type":"postback",
                        "title":"Ver bebidas",
                        "payload": "Restaurante@BO_Produto::verBebidas"
                      }
                    ]
                  }
                }
              }
        }';
                break;
            case BO_Tipo_produto::PRATO:
            $jsonData = '{
            "messaging_type": "RESPONSE",
            "recipient":{
                "id": "' . Registry::get('senderId') . '"
            }, 
            "message":{
                "attachment":{
                  "type":"template",
                  "payload":{
                    "template_type":"button",
                    "text":"'.utf8_encode($texto).'",
                    "buttons":[
                      {
                        "type":"postback",
                        "title":"Adicionar outro",
                        "payload": "Restaurante@BO_Produto::cadastrarPrato"
                      },
                      {
                        "type":"postback",
                        "title":"Remover '.utf8_encode('�ltimo').'",
                        "payload": "Restaurante@BO_Produto::removerUltimoProduto?'.$idProduto.'"
                      },
                      {
                        "type":"postback",
                        "title":"Ver '.$label.'",
                        "payload": "Restaurante@BO_Produto::verPratos"
                      }
                    ]
                  }
                }
              }
        }';
            break;

        }

        return $jsonData;
    }


    public static function getMenuCadastrar(){
        $jsonData = '{
            "messaging_type": "RESPONSE",
            "recipient":{
                "id": "' . Registry::get('senderId') . '"
            }, 
            "message":{
                "attachment":{
                  "type":"template",
                  "payload":{
                    "template_type":"button",
                    "text":"'.utf8_encode("Que tipo de produto deseja cadastrar para venda?").'",
                    "buttons":[
                      {
                        "type":"postback",
                        "title":"Prato",
                        "payload": "Restaurante@BO_Produto::cadastrarPrato"
                      },
                      {
                        "type":"postback",
                        "title":"Bebida",
                        "payload": "Restaurante@BO_Produto::cadastrarBebida"
                      }
                    ]
                  }
                }
              }
        }';
        return $jsonData;
    }
}