<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 19/04/2018
 * Time: 18:13
 */
class BO_Estado_pedido
{
    const REALIZANDO = 1;
    const AGUARDANDO_APROVACAO = 2;
    const APROVADO = 3;
    const SAIU_PARA_ENTREGA= 4;
    const ENTREGUE = 5;
    const FOI_PAGO= 6;
    const NAO_FOI_PAGO = 7;
    const CANCELADO_PELO_CLIENTE = 8;
    const CANCELADO_PELA_EMPRESA = 9;

    public static function getDescricao($idEstadoPedido){
        switch ($idEstadoPedido){
            case BO_Estado_pedido::REALIZANDO:
                return "realizando";
            case BO_Estado_pedido::AGUARDANDO_APROVACAO:
                return "aguardando aprova��o";
            case BO_Estado_pedido::APROVADO:
                return "aprovado";
            case BO_Estado_pedido::SAIU_PARA_ENTREGA:
                return "saiu para entrega";
            case BO_Estado_pedido::ENTREGUE:
                return "entregue";
            case BO_Estado_pedido::FOI_PAGO:
                return "foi pago";
            case BO_Estado_pedido::NAO_FOI_PAGO:
                return "n�o foi pago";
            case BO_Estado_pedido::CANCELADO_PELO_CLIENTE:
                return "cancelado pelo cliente";
            case BO_Estado_pedido::CANCELADO_PELA_EMPRESA:
                return "cancelado pela empresa";
        }
        return null;
    }
}