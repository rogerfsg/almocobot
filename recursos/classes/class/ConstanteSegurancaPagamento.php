<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Constante_Seguranca_Pagamento
 *
 * @author home
 */
class ConstanteSegurancaPagamento {
    //put your code here
    
    //lista de paginas que nao precisam de autenticacao
    public static $paginasComAutenticacao = array("formas_pagamento", "finalizacao_pedido",
        "meus_horarios", "minhas_cobrancas", "visualizar_cobranca", "alterar_senha",
        "assinaturas_do_cliente");
    
    //actions que nao precisam de autenticacao
    public static $actionsComAutenticacao = array(
        "alterarSenha",
        "processarPedido");
    public static $paginasSemTopo = array("venda_corrigir_cupom", "omega_empresa_web",
        "possuo_grupo", "nao_lembro_meu_grupo", "meus_grupos", "ainda_nao_possuo_grupo",
        "cadastrar_grupo", "pacotes_pelo_sistema");

    public static $paginasSemAutenticacao = array(array("paginas", "logar_pelo_sistema"));

}
