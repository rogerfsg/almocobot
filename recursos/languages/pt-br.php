<?

define('MENSAGEM_AVISO_RELATORIO_ABERTO_EM_POPUP', "O relat�rio est� sendo aberto em uma janela popup.
                                                    Caso o bloqueador de popups esteja ativado, desative-o e atualize a p�gina.");

define('MENSAGEM_EM_CONSTRUCAO', "");

define('MENSAGEM_LOGOUT', "Logout realizado com sucesso.");

define('MENSAGEM_EXPIRACAO_SESSAO', "Seu tempo de sess�o expirou. Por seguran�a, fa�a o login novamente.");

define('MENSAGEM_PARA_SALVAR_ALTERACOES', "Para salvar as altera��es feitas");

define('MENSAGEM_CLIQUE_AQUI', "clique aqui");

define('ASSUNTO_DO_EMAIL_DE_REENVIO_SENHA', "Reenvio de senha - " . TITULO_PAGINAS);

define('MENSAGEM_EMAIL_REENVIO_SENHA_SUCESSO', "O email com a sua senha foi enviado com sucesso.");

define('MENSAGEM_EMAIL_REENVIO_SENHA_ERRO', "N�o foi poss�vel encontrar o email especificado em nosso banco de dados.");

define('MENSAGEM_ALTERAR_POSICAO_FUNCAO', "Aten��o, ao alterar a posi��o da fun��o, automaticamente todas as vagas associadas ser�o realocadas.");

define('MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO', "Voc� n�o tem autoriza��o para acessar esta �rea.");

define('MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO_ACAO', "Voc� n�o tem autoriza��o para executar esta a��o.");

define('LABEL_BOTAO_DE_IMPRESSAO', "Vers�o para Impress�o");

?>