<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

?>

<!DOCTYPE html>
<html>
<head>
    <title><?= TITULO_PAGINAS_CLIENTE ?></title>

    <?= Helper::getTagDoFavicon(); ?>
    <?= Helper::carregarCssFlatty(); ?>

    <?= Javascript::importarTodasAsBibliotecas(); ?>

</head>
<body class='contrast-red error contrast-background'>
<div class='middle-container'>
    <div class='middle-row'>
        <div class='middle-wrapper'>
            <div class='error-container-header'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                <i class='icon-exclamation-sign'></i>
                                500
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='error-container'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-4 col-sm-offset-4'>
                            <h4 class='text-center title'>
                                Ooops! Alguma coisa deu errado.
                                <br>
                                Ocorreu um problema inesperado, sera corrigido em breve.
                            </h4>

                            <p class='text-center'>
                                <small>You can</small>
                            </p>
                            <form action='search_results.html' method='get'>
                                <div class='form-group'>
                                    <div class='input-group controls-group'>
                                        <input value="" placeholder="Search..." class="form-control" name="q"
                                               type="text"/>
                                        <span class='input-group-btn'>
                          <button class='btn' type='submit'>
                              <i class='icon-search'></i>
                          </button>
                        </span>
                                    </div>
                                </div>
                            </form>
                            <p class='text-center'>
                                <small>or</small>
                            </p>
                            <div class='text-center'>
                                <a class='btn btn-md btn-ablock' href='index.html'>
                                    <i class='icon-chevron-left'></i>
                                    Voltar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='error-container-footer'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                <img width="121" height="31" alt="Flatty" src="assets/images/logo_lg.svg"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= Javascript::importarBibliotecasFlatty(); ?>
</body>

</html>
